from bson.objectid import ObjectId
from bson.dbref import DBRef

house = {
  'address':'',
  'price':'',
  'property information':'',
  'main':{
    'size':{
      'bedrooms':'',
      'bathrooms':'',
      'garage':'',
      'square ft':'',
      'square metre':'',
      'lot size':'',
    },
    'value':{
      'price per sq/ft for this property':'',
      'average price per sq/ft for this area':'',
      'median home price':'',
    },
    'extra':{
      'year built':'',
      'school district':'',
      'mls number':'',
      'live date':'',
      'property type':'',
      'area location':'',
      'subdivision':'',
      'county':'',
      'status':'',
      'map page':'',
    }
  },
  'general':{
    'rooms':'',
    'stories':'',
    'bedrooms':'',
    'full bathrooms':'',
    'garage capacity':'',
    'parking description':'',
    'fireplaces':'',
    'square footage':'',
  },
  'exterior comments':{
    'exterior features':'',
    'fence':'',
    'fence description':'',
    'pool':'',
  },
  'interior comments':{
    'dining room level':'',
    'flooring type':'',
    'interior features':'',
    'kitchen level':'',
    'living room level':'',
    'rooms other':'',
  },
  'construction':{
    'exterior siding':'',
    'foundation type':'',
    'new construction':'',
    'style':'',
    'year built':'',
  },
  'equipment':{
    'appliances':'',
    'cooling':'',
    'heating':'',
    'equipment':'',
    'water heater type':'',
    'sewer type':'',
    'water type':'',
  },
  'property':{
    'listing agent':'',
    'listing provided courtesy of':'',
    'mls number':'',
  },
  'lot comments':{
    'view description':'',
    'waterfront description':'',
  },
  'additional information':{
    'high school':'',
    'tax amount':'',
  },
}


form = (
    # --- sample start --- #
    ('address', #title of this step

      {'address':('en',)}, #set language attrs as sets package(single child also end with comma)
      ('title', 'geolocation', 'property information'), #normal attar
      {'state': ('en', 'abbr', 'cn')},
      {'county': ('en', 'cn')},
      {'area location': ('en', 'cn')},
      {'subdivision': ('en', 'cn')},
      {'city': ('en', 'cn')},
      {'town': ('en', 'cn')},
      ),
    ('price',
      {'price': ('en', 'cn')},
      ('median home price',
        'price per sq/ft for this property',
        'average price per sq/ft for this area',
        'tax amount',
        'common charge',
        'hoa dues')
      ),
    #('price history', #type include: 售出，上市，价格更改
      #{'item1':('date','type','price')},
      #{'item2':('date','type','price')},
      #),
    ('size',
      ('square footage',
        'square metre',
        'lot size')
      ),
    # --- sample end --- #
    ('construction', #房屋建筑信息
      ('room', #房间数
        'floor', #楼层数
        'bedroom', #卧室数
        'bathroom', #卫生间/厕所数
        'garage', #车库数
        'style', #建筑风格
        'foundation', #地基材料
        'external wall', #外墙材料
        'drain', #排水类型
        'water supply') #供水类型
      ),
    ('internal', #房屋内部装修信息
      ('dinning room floor', #餐厅楼层
        'kitchen floor', #厨房楼层
        'living room', #客厅楼层
        'ground', #地面材料
        'internal facility', #内部设施
        'else internal facility') #其他屋内设施
      ),
    ('external', #房屋外部设置
        ('terrace', #露台
          'lawn', #草坪
          'watering can', #草坪上洒水器
          'pump', #水泵
          'well', #水井
          'fencing', #木质围栏
          'pool') #泳池
        ),
    ('appliance', #家电及设施
        ('household appliance', #家用电器
          'refrigerator', #制冷设备
          'warmer', #制热设备
          'water warmer', #水加热系统
          'else appliance')
        ),
    ('periphery', #周边地块信息
        ('tree', #林木
          'water pool', #水池
          'hill') #小山
        ),
    ('school', #学区信息
        ('primary', #小学
          'middle', #中学
          'high', #高中
          'college') #大学
        ),
    ('estate information', #地产信息
        ('construction year', #建筑年份
          'MLS', #MLS号码
          'school area', #学区
          'sale start time', #始售时间
          'estate type', #地产类型
          'sale status', #销售情况
          'sale agent', #销售代理
          'sale info provider') #上市信息提供
        )
    )

def form2dict():
  ret = {}
  def process(item):
    n = type(item).__name__
    if n == 'tuple':
      ret.update(**{i:None for i in item})
    elif n == 'dict':
      ret.update(**{k:{i:None for i in v} for k,v in item.items()})
    elif n == 'str':
      ret.update(**{item:None})
  for y in form:
    for x in y[1:]:
      process(x)
  return ret

def get_agent(handler, amand=None):
  aid = handler.get_argument('agent', None)
  if aid:
    oid = DBRef('agent', ObjectId(aid))
    if amand:
      amand['agent'] = oid
      return
    return oid
