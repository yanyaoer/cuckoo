import functools
import urllib.parse as urlparse
from urllib.parse import urlencode
from tornado.httpclient import HTTPError

def authenticated(method):
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if not self.current_user:
            if self.request.method in ("GET", "HEAD"):
                url = self.get_login_url()
                if "?" not in url:
                    if urlparse.urlsplit(url).scheme:
                        # if login url is absolute, make next absolute too
                        next_url = self.request.full_url()
                    else:
                        next_url = self.request.uri
                    url += "?" + urlencode(dict(next=next_url))
                self.redirect(url)
                return
            raise HTTPError(403)
        return method(self, *args, **kwargs)
    return wrapper

def require_role(*roles):
  def wrapper(method):
    @functools.wraps(method)
    def wrapped(self, *args, **kwargs):
      print(get_current_user_role(self), roles)
      if get_current_user_role(self) not in roles:
        raise HTTPError(403)
      return method(self, *args, **kwargs)
    return wrapped
  return wrapper

def get_current_user_role(self):
  admin = self.db.admin.find_one(self.current_user)
  if admin:
    return admin.get('role', None)
