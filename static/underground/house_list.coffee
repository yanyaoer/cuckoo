$ = require('semantic-ui')


$('.ui.checkbox').checkbox().click (e)->
  e.preventDefault()
  el = $(this)
  $.get el.attr('url'), (d)->
    json = JSON.parse(d)
    label = el.find('label')
    if 'error' of json
      label.text(json.error)
    else
      label.text(json.text)
