$ = require('semantic-ui')
ed = require('ed')

insertIMG = (img)->
  s = window.getSelection()
  n = s.baseNode
  c = $('#editor .ed-content')
  if $.contains(c[0], s)
    s.getRangeAt(0).insertNode(img)
  else
    c.append img

editor = ed($('#editor')[0],{
  path: '/underground/file'
  resolve: (res)->
    return JSON.parse(res)
  callback: (e, res)->
    insertIMG $("<img src='/assets/#{res[0]}' alt='' />")
})


$('#article-form').submit (e)->
  e.preventDefault()
  el = $(this)
  $.ajax
    type: el.attr('method')
    url: el.attr('action')
    data:
      title: el.find('[name=title]').val()
      tag: el.find('[name=tag]').val()
      agent: el.find('[name=agent]').val()
      content: editor.value()
  .done (res)->
    window.location.href= "/underground/article/#{JSON.parse(res)._id}"
