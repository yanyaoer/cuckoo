// Generated by CoffeeScript 1.7.1
(function() {
  var $, ed, editor, insertIMG;

  $ = require('semantic-ui');

  ed = require('ed');

  insertIMG = function(img) {
    var c, n, s;
    s = window.getSelection();
    n = s.baseNode;
    c = $('#editor .ed-content');
    if ($.contains(c[0], s)) {
      return s.getRangeAt(0).insertNode(img);
    } else {
      return c.append(img);
    }
  };

  editor = ed($('#editor')[0], {
    path: '/underground/file',
    resolve: function(res) {
      return JSON.parse(res);
    },
    callback: function(e, res) {
      return insertIMG($("<img src='/assets/" + res[0] + "' alt='' />"));
    }
  });

  $('#article-form').submit(function(e) {
    var el;
    e.preventDefault();
    el = $(this);
    return $.ajax({
      type: el.attr('method'),
      url: el.attr('action'),
      data: {
        title: el.find('[name=title]').val(),
        tag: el.find('[name=tag]').val(),
        agent: el.find('[name=agent]').val(),
        content: editor.value()
      }
    }).done(function(res) {
      return window.location.href = "/underground/article/" + (JSON.parse(res)._id);
    });
  });

}).call(this);

//# sourceMappingURL=article.map
