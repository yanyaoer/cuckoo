$ = require('semantic-ui')
select = require('select')

key = "AheyINv5pZOZgYhxGYmMttiF106XCbuWodZf_DMD5x-SWQl2OzAfywT_9wCYCZa4"

window.geo_cb = (res)->
  try
    list = res.resourceSets[0].resources[0].point.coordinates
    $('#house-form input[name="geolocation"]').val(list.toString())
  catch e
    alert "fetch geolocation error, try later"

$('#house-form').submit (e)->
  e.preventDefault()
  el = $(this)
  $.ajax
    url: el.attr('action')
    type: el.attr('method')
    data: $(this).serialize()
  .done (res)->
    window.location.href= JSON.parse(res).redir

gf = $('#gallery-form')
gf.find('input[type=file]').change (e)->
  gf.submit()


$('#update-geo').click (e)->
  e.preventDefault()
  addr = $('#house-form input[name="address.en"]').val()
  uri = "http://dev.virtualearth.net/REST/v1/Locations" +
      "?query=#{encodeURI(addr)}&output=json&jsonp=geo_cb&key=#{key}"
  $.getScript uri

do_select = (sel, menu, multi)->
  el = $ sel
  el.attr('type', 'hidden')
  s = select()
    .on 'change', ->
      el.val(s.values())
  for item in menu
    s.add item
  if multi
    s.multiple()
    if el.val()
      s.select x for x in el.val().split(',') when x isnt ''
  if el.val() and el.val() in menu
    s.select el.val()
  el.parent().append s.el
  return s


config =
  multi:
    'input[name=refrigerator]': [
      '中央空调'
      '窗式空调'
      '墙式空调'
      '独立式空调'
    ]
    'input[name=warmer]': [
      '楼内供暖'
      '热风供暖'
      '低温地板辐射供暖'
      '热水供暖系统'
    ]
    'input[name="water warmer"]': [
      '燃气'
      '燃油'
      '太阳能'
      '地热'
    ]
    'input[name=foundation]': [
      '地下室-装修完成'
      '地下室-半装修'
      '地下室-未装修'
      '槽隙'
      '水泥板'
    ]
    'input[name=ground]': [
      '实木地板'
      '木复合地板'
      '地毯'
      '瓷砖'
    ]
  single:
    'input[name="estate type"]': [
      '独户住宅'
      '独户产权住宅'
      '多户住宅'
      '联体别墅'
      '共管公寓'
      '集体合作公寓'
    ]
    'input[name="sale status"]': [
      '在售'
      '降价'
      '已售'
      '退市'
    ]

do_select k,v,true for k,v of config.multi
do_select k,v,false for k,v of config.single

do_select 'input[name=state]', state_data, false
