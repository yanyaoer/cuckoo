// Generated by CoffeeScript 1.7.1
(function() {
  (function(factory) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["backbone"], factory);
    } else if (typeof exports === "object") {
      return module.exports = factory(require("backbone"));
    } else {
      return factory(window.Backbone);
    }
  })(function(Backbone) {
    "use strict";
    var loadUrl;
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        return (i[r].q = i[r].q || []).push(arguments);
      };
      i[r].l = 1 * new Date();
      a = s.createElement(o);
      m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      return m.parentNode.insertBefore(a, m);
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    window._hmt = [];
    (function() {
      var hm, s;
      hm = document.createElement("script");
      hm.src = "//hm.baidu.com/hm.js?6e18f9ee8c1a3a4fc0f10e796d8414c4";
      s = document.getElementsByTagName("script")[0];
      return s.parentNode.insertBefore(hm, s);
    })();
    ga('create', 'UA-52545881-1', 'auto');
    loadUrl = Backbone.History.prototype.loadUrl;
    return Backbone.History.prototype.loadUrl = function(fragmentOverride) {
      var ga, gaFragment, matched;
      matched = loadUrl.apply(this, arguments);
      gaFragment = this.fragment;
      if (!/^\//.test(gaFragment)) {
        gaFragment = "/" + gaFragment;
      }
      if (typeof window._gaq !== "undefined") {
        window._gaq.push(["_trackPageview", gaFragment]);
      }
      window._hmt.push(["_trackPageview", gaFragment]);
      ga = void 0;
      if (window.GoogleAnalyticsObject && window.GoogleAnalyticsObject !== "ga") {
        ga = window.GoogleAnalyticsObject;
      } else {
        ga = window.ga;
      }
      if (typeof ga !== "undefined") {
        ga("send", "pageview", gaFragment);
      }
      return matched;
    };
  });

}).call(this);

//# sourceMappingURL=backbone.analytics.map
