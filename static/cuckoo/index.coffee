require('es5-shim')
$ = require('jquery')
B = require('backbone')
B.$ = $
require('./backbone.analytics')
M = require('backbone.marionette')
M.$ = $
Nanobar = require('nanobar')
qs = require('query-string')
handler = require('./handler')
tpl = require('./tpl')
U = require('./util')


router = M.AppRouter.extend
  controller: handler
  appRoutes:
    'login': 'login'
    'logout': 'logout'
    'register': 'register'
    'search': 'search'
    'article/': 'article_list'
    'article/:id': 'article_single'
    'house/': 'house_list'
    'house/gallery/:id': 'house_gallery'
    'house/neighbor/:id': 'house_neighbor'
    'house/:id': 'house'
    '': 'index'
    '*default': 'default'

if !window.console
  window.console =
    log: (msg)->
      return msg

window.cuckoo = new M.Application()

cuckoo.on 'start', ()->
  window.R = new router()
  nanobar = new Nanobar()

  $('#wrap')
    .find('.search input[type=text]')
    .val qs.parse(window.location.search)['q'] || ''

  cuckoo.addRegions
    contain: '#contain'
    tip: '#tip'

  B.history.start
    pushState: true
    root:'/'

  U.search()
  $(document)
  .on 'click', 'a[href^="/"]', (e)->
    e.preventDefault()
    R.navigate $(e.currentTarget).attr('href').substr(1), {trigger: true}
    window.scrollTo(0,0)
  .ajaxStart ()->
    nanobar.infinite()
  .ajaxStop ()->
    nanobar.dismiss()
  .bind 'ajaxError', (e, err)->
    nanobar.dismiss()
    U.tip(err.responseText)

$ ->
  cuckoo.start()
#module.exports = cuckoo
