var jade = require("jade"); module.exports = { "article_form": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (article) {
buf.push("<h2 class=\"ui header\">share knowledge:</h2>");
if ( article)
{
var act = article._id
var mtd = 'PUT'
}
else
{
var act = ''
var mtd = 'POST'
}
buf.push("<form id=\"article-form\"" + (jade.attr("method", mtd, true, false)) + (jade.attr("action", "/api/article/" + (act) + "", true, false)) + " class=\"ui\"><div class=\"ui form\"><div class=\"field\"><input type=\"text\" name=\"title\" placeholder=\"title\"" + (jade.attr("value", article.title || '', true, false)) + "/></div><div id=\"editor\" class=\"field\"></div><div class=\"field\"><input type=\"text\" name=\"tag\" placeholder=\"multi-tag split with comma, eg:usa, announce\"" + (jade.attr("value", article.tag?article.tag.join(','):'', true, false)) + "/></div><button id=\"submit\" class=\"ui blue submit button\">submit</button></div></form>");}("article" in locals_for_with?locals_for_with.article:typeof article!=="undefined"?article:undefined));;return buf.join("");
}, "article_list": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (article_list, _, type) {
buf.push("<div class=\"article-list\">");
// iterate article_list
;(function(){
  var $$obj = article_list;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var article = $$obj[idx];

buf.push("<div class=\"article\">");
var agent = article.agent
if ( agent)
{
buf.push("<div class=\"agent-photo\"><img" + (jade.attr("src", "/assets/" + (agent.photo[0]) + "/100x100", true, false)) + "/><p>" + (jade.escape((jade_interp = agent.name) == null ? '' : jade_interp)) + " [" + (jade.escape((jade_interp = agent.title) == null ? '' : jade_interp)) + "]</p></div>");
}
buf.push("<h2 class=\"hd\"><a" + (jade.attr("href", "/article/" + (article._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = article.title) ? "" : jade_interp)) + "</a></h2><div class=\"bd\">" + (jade.escape(null == (jade_interp = article.content.replace(/(<([^>]+)>)/ig,"").substring(0,150)) ? "" : jade_interp)) + "...</div><div class=\"ft\">");
if ( article.tag && article.tag.length != 0)
{
buf.push("<span class=\"tag-label\">" + (jade.escape(null == (jade_interp = _('tag')) ? "" : jade_interp)) + ":</span>");
// iterate article.tag
;(function(){
  var $$obj = article.tag;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var t = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/search?t=" + (type || 'article') + "&q=tag:" + (t) + "", true, false)) + " class=\"tag\">" + (jade.escape(null == (jade_interp = t) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var t = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/search?t=" + (type || 'article') + "&q=tag:" + (t) + "", true, false)) + " class=\"tag\">" + (jade.escape(null == (jade_interp = t) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

}
buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var article = $$obj[idx];

buf.push("<div class=\"article\">");
var agent = article.agent
if ( agent)
{
buf.push("<div class=\"agent-photo\"><img" + (jade.attr("src", "/assets/" + (agent.photo[0]) + "/100x100", true, false)) + "/><p>" + (jade.escape((jade_interp = agent.name) == null ? '' : jade_interp)) + " [" + (jade.escape((jade_interp = agent.title) == null ? '' : jade_interp)) + "]</p></div>");
}
buf.push("<h2 class=\"hd\"><a" + (jade.attr("href", "/article/" + (article._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = article.title) ? "" : jade_interp)) + "</a></h2><div class=\"bd\">" + (jade.escape(null == (jade_interp = article.content.replace(/(<([^>]+)>)/ig,"").substring(0,150)) ? "" : jade_interp)) + "...</div><div class=\"ft\">");
if ( article.tag && article.tag.length != 0)
{
buf.push("<span class=\"tag-label\">" + (jade.escape(null == (jade_interp = _('tag')) ? "" : jade_interp)) + ":</span>");
// iterate article.tag
;(function(){
  var $$obj = article.tag;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var t = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/search?t=" + (type || 'article') + "&q=tag:" + (t) + "", true, false)) + " class=\"tag\">" + (jade.escape(null == (jade_interp = t) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var t = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/search?t=" + (type || 'article') + "&q=tag:" + (t) + "", true, false)) + " class=\"tag\">" + (jade.escape(null == (jade_interp = t) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

}
buf.push("</div></div>");
    }

  }
}).call(this);

buf.push("<div class=\"pager\"></div></div>");}("article_list" in locals_for_with?locals_for_with.article_list:typeof article_list!=="undefined"?article_list:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined,"type" in locals_for_with?locals_for_with.type:typeof type!=="undefined"?type:undefined));;return buf.join("");
}, "article_single": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (article, _, type) {
buf.push("<div class=\"breadcrumb\"><a href=\"/article/\" class=\"section\">article</a>/<span class=\"active\">" + (jade.escape(null == (jade_interp = article.title) ? "" : jade_interp)) + "</span></div><div class=\"article-single\"><div class=\"article-content\"><h1 class=\"hd\">" + (jade.escape(null == (jade_interp = article.title) ? "" : jade_interp)) + "</h1><div class=\"bd\">" + (null == (jade_interp = article.content) ? "" : jade_interp) + "</div></div><div class=\"article-side\">");
var agent = article.agent
if ( agent)
{
buf.push("<div class=\"agent-photo\"><img" + (jade.attr("src", "/assets/" + (agent.photo[0]) + "/100x100", true, false)) + "/><p>" + (jade.escape((jade_interp = agent.name) == null ? '' : jade_interp)) + " [" + (jade.escape((jade_interp = agent.title) == null ? '' : jade_interp)) + "]</p></div>");
}
if ( article.tag && article.tag.length != 0)
{
buf.push("<span class=\"tag-label\">" + (jade.escape(null == (jade_interp = _('tag')) ? "" : jade_interp)) + ":</span>");
// iterate article.tag
;(function(){
  var $$obj = article.tag;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var t = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/search?t=" + (type || 'article') + "&q=tag:" + (t) + "", true, false)) + " class=\"tag\">" + (jade.escape(null == (jade_interp = t) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var t = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/search?t=" + (type || 'article') + "&q=tag:" + (t) + "", true, false)) + " class=\"tag\">" + (jade.escape(null == (jade_interp = t) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

}
buf.push("</div></div>");}("article" in locals_for_with?locals_for_with.article:typeof article!=="undefined"?article:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined,"type" in locals_for_with?locals_for_with.type:typeof type!=="undefined"?type:undefined));;return buf.join("");
}, "base": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (_, encodeURIComponent, window) {
buf.push("<header><div class=\"inner\"><a href=\"/\" class=\"logo\"><img src=\"/static/img/logo.png\"" + (jade.attr("alt", "" + (_('sitename')) + "-logo", true, false)) + (jade.attr("title", "" + (_('slogan')) + "", true, false)) + "/></a><form action=\"/search\" method=\"GET\" class=\"search\"><div id=\"search-label\" class=\"nav\"><label for=\"house\" class=\"active\">" + (jade.escape(null == (jade_interp = _('house')) ? "" : jade_interp)) + "</label><label for=\"article\">" + (jade.escape(null == (jade_interp = _('article')) ? "" : jade_interp)) + "</label><a href=\"/search?t=article&amp;q=\">购房知识库</a></div><div class=\"inner\"><input id=\"search-type\" type=\"hidden\" name=\"t\" value=\"house\"/><input type=\"text\" name=\"q\"/><button type=\"submit\">" + (jade.escape(null == (jade_interp = _('search')) ? "" : jade_interp)) + "</button></div><div class=\"hot\"><span>热门地区:</span><a href=\"/search?t=house&amp;state=CA\">加州</a><a href=\"/search?t=house&amp;state=NY\">纽约</a>");
// iterate ['阳光', '学区房', '度假']
;(function(){
  var $$obj = ['阳光', '学区房', '度假'];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var q = $$obj[$index];

buf.push("<a" + (jade.attr("href", "/search?t=house&q=" + (encodeURIComponent(q)) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = q) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var q = $$obj[$index];

buf.push("<a" + (jade.attr("href", "/search?t=house&q=" + (encodeURIComponent(q)) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = q) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></form><div class=\"user\">");
if ( window.user)
{
buf.push("hi, " + (jade.escape((jade_interp = window.user.email) == null ? '' : jade_interp)) + "<a href=\"/logout\">logout</a>");
if ( window.user.admin)
{
buf.push("<a href=\"/underground/\">underground</a>");
}
}
else
{
buf.push("<a href=\"/login\" class=\"btn\">" + (jade.escape(null == (jade_interp = _('login')) ? "" : jade_interp)) + "</a><a href=\"/register\" class=\"btn\">" + (jade.escape(null == (jade_interp = _('register')) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></header><div id=\"contain\" class=\"main\"></div>");}("_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined,"encodeURIComponent" in locals_for_with?locals_for_with.encodeURIComponent:typeof encodeURIComponent!=="undefined"?encodeURIComponent:undefined,"window" in locals_for_with?locals_for_with.window:typeof window!=="undefined"?window:undefined));;return buf.join("");
}, "house": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (house, window, _, size, hide, encodeURIComponent) {
buf.push("<div class=\"house-single\"><div class=\"house-main\"><h1 class=\"hd\">" + (jade.escape(null == (jade_interp = house.address.en) ? "" : jade_interp)) + "</h1><div class=\"desc\">" + (jade.escape(null == (jade_interp = house['property information']) ? "" : jade_interp)) + "</div><nav class=\"house-tab\">");
var summary='', gallery='', neighbor=''
var loc = window.location.pathname.split('/')
var loc_path = loc[2]
if (loc.length==3) summary='active'
if (loc_path=='gallery') gallery='active'
if (loc_path=='neighbor') neighbor='active'
buf.push("<a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + (jade.cls([summary], [true])) + ">" + (jade.escape(null == (jade_interp = _('summary')) ? "" : jade_interp)) + "</a><a" + (jade.attr("href", "/house/gallery/" + (house._id) + "", true, false)) + (jade.cls([gallery], [true])) + ">" + (jade.escape(null == (jade_interp = _('gallery')) ? "" : jade_interp)) + "</a></nav></div><div class=\"house-boxs\"><div class=\"hbox slider\">");
if ( house.gallery)
{
buf.push("<img id=\"cover\"" + (jade.attr("src", "/assets/" + (house.gallery[0]._id) + "/490x325", true, false)) + "/><i class=\"move-left\">&lt;</i><div class=\"thumb\"><div class=\"inner\">");
var i = 0, s = size || '/80x56'
// iterate house.gallery
;(function(){
  var $$obj = house.gallery;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id+s) + "", true, false)) + (jade.cls([(i==0)?"active":''], [true])) + "/>");
i++
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id+s) + "", true, false)) + (jade.cls([(i==0)?"active":''], [true])) + "/>");
i++
    }

  }
}).call(this);

buf.push("</div></div><i class=\"move-right\">&gt;</i>");
}
else
{
buf.push("<p>gallery coming soon...</p>");
}
buf.push("</div>");
// iterate ['construction', 'internal', 'external', 'periphery', 'estate information']
;(function(){
  var $$obj = ['construction', 'internal', 'external', 'periphery', 'estate information'];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var title = $$obj[$index];

var box = window.base.house[title]
buf.push("<div" + (jade.cls(['hbox',title], [null,true])) + "><div class=\"hd\">" + (jade.escape(null == (jade_interp = _(title)) ? "" : jade_interp)) + "</div><div class=\"bd\">");
// iterate box
;(function(){
  var $$obj = box;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  }
}).call(this);

buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var title = $$obj[$index];

var box = window.base.house[title]
buf.push("<div" + (jade.cls(['hbox',title], [null,true])) + "><div class=\"hd\">" + (jade.escape(null == (jade_interp = _(title)) ? "" : jade_interp)) + "</div><div class=\"bd\">");
// iterate box
;(function(){
  var $$obj = box;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  }
}).call(this);

buf.push("</div></div>");
    }

  }
}).call(this);

buf.push("</div><div class=\"house-boxs\"><div class=\"hbox summary\"><div id=\"address\" class=\"hd\">" + (jade.escape(null == (jade_interp = house.address.en) ? "" : jade_interp)) + "</div><div class=\"bd\"><div class=\"house-summary\"><p class=\"grey\"><span>" + (jade.escape((jade_interp = _('price')) == null ? '' : jade_interp)) + ": </span><em class=\"price\">" + (jade.escape((jade_interp = house.price.cn || 'N/A') == null ? '' : jade_interp)) + "</em>美元</p><p><span class=\"grey\">" + (jade.escape((jade_interp = _('size')) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house['square metre'] || 'N/A') == null ? '' : jade_interp)) + "平方米</p><p><span class=\"grey\">" + (jade.escape((jade_interp = _('construction year')) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house['construction year'] || 'N/A') == null ? '' : jade_interp)) + "年</p><ul class=\"compact\">");
var fields = hide ? ['bedroom', 'bathroom'] : ['floor', 'room', 'bedroom', 'bathroom']
// iterate fields
;(function(){
  var $$obj = fields;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var f = $$obj[$index];

buf.push("<li><span class=\"grey\">" + (jade.escape((jade_interp = _(f)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[f] || 'N/A') == null ? '' : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var f = $$obj[$index];

buf.push("<li><span class=\"grey\">" + (jade.escape((jade_interp = _(f)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[f] || 'N/A') == null ? '' : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div>");
var agent = house.agent
if ( agent)
{
buf.push("<div class=\"agent\">");
if ( agent)
{
buf.push("<div class=\"agent-photo\"><img" + (jade.attr("src", "/assets/" + (agent.photo[0]) + "/100x100", true, false)) + "/><p>" + (jade.escape((jade_interp = agent.name) == null ? '' : jade_interp)) + " [" + (jade.escape((jade_interp = agent.title) == null ? '' : jade_interp)) + "]</p></div>");
}
buf.push("<div class=\"agent-quote\">" + (jade.escape(null == (jade_interp = house['property information']) ? "" : jade_interp)) + "</div></div>");
}
buf.push("</div></div><div class=\"hbox map\"><div class=\"hd\">" + (jade.escape(null == (jade_interp = _('map')) ? "" : jade_interp)) + "</div><div id=\"map\" style=\"position:relative;\" class=\"bd\"></div></div>");
var box = window.base.house.price
buf.push("<div class=\"hbox price\"><div class=\"hd\">" + (jade.escape(null == (jade_interp = _('price')) ? "" : jade_interp)) + "</div><div class=\"bd\">");
// iterate box
;(function(){
  var $$obj = box;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string' && house[k])
{
buf.push("<p>");
var val = (typeof(house[k]) == 'number') ? '$'+house[k]+'美元' : house[k]
buf.push("<span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = val) == null ? '' : jade_interp)) + " </p>");
}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string' && house[k])
{
buf.push("<p>");
var val = (typeof(house[k]) == 'number') ? '$'+house[k]+'美元' : house[k]
buf.push("<span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = val) == null ? '' : jade_interp)) + " </p>");
}
    }

  }
}).call(this);

    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string' && house[k])
{
buf.push("<p>");
var val = (typeof(house[k]) == 'number') ? '$'+house[k]+'美元' : house[k]
buf.push("<span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = val) == null ? '' : jade_interp)) + " </p>");
}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string' && house[k])
{
buf.push("<p>");
var val = (typeof(house[k]) == 'number') ? '$'+house[k]+'美元' : house[k]
buf.push("<span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = val) == null ? '' : jade_interp)) + " </p>");
}
    }

  }
}).call(this);

    }

  }
}).call(this);

buf.push("</div></div>");
// iterate ['size', 'appliance', 'school']
;(function(){
  var $$obj = ['size', 'appliance', 'school'];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var title = $$obj[$index];

var box = window.base.house[title]
buf.push("<div" + (jade.cls(['hbox',encodeURIComponent(title)], [null,true])) + "><div class=\"hd\">" + (jade.escape(null == (jade_interp = _(title)) ? "" : jade_interp)) + "</div><div class=\"bd\">");
// iterate box
;(function(){
  var $$obj = box;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  }
}).call(this);

buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var title = $$obj[$index];

var box = window.base.house[title]
buf.push("<div" + (jade.cls(['hbox',encodeURIComponent(title)], [null,true])) + "><div class=\"hd\">" + (jade.escape(null == (jade_interp = _(title)) ? "" : jade_interp)) + "</div><div class=\"bd\">");
// iterate box
;(function(){
  var $$obj = box;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

if ( typeof(field)=='string')
{
if ( house[field])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(field)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[field]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate field
;(function(){
  var $$obj = field;
  if ('number' == typeof $$obj.length) {

    for (var v = 0, $$l = $$obj.length; v < $$l; v++) {
      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var v in $$obj) {
      $$l++;      var k = $$obj[v];

if ( typeof(k) =='string')
{
if ( house[k])
{
buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = _(k)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[k]||'N/A') == null ? '' : jade_interp)) + "</p>");
}
}
else
{
// iterate k
;(function(){
  var $$obj = k;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<p><span class=\"grey\">" + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[v][item]||'N/A') == null ? '' : jade_interp)) + "</p>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
    }

  }
}).call(this);

buf.push("</div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");}("house" in locals_for_with?locals_for_with.house:typeof house!=="undefined"?house:undefined,"window" in locals_for_with?locals_for_with.window:typeof window!=="undefined"?window:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined,"size" in locals_for_with?locals_for_with.size:typeof size!=="undefined"?size:undefined,"hide" in locals_for_with?locals_for_with.hide:typeof hide!=="undefined"?hide:undefined,"encodeURIComponent" in locals_for_with?locals_for_with.encodeURIComponent:typeof encodeURIComponent!=="undefined"?encodeURIComponent:undefined));;return buf.join("");
}, "house_gallery": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (house, window, _) {
buf.push("<div class=\"house-single house-gallery\"><div class=\"house-main\"><h1 class=\"hd\">" + (jade.escape(null == (jade_interp = house.address.en) ? "" : jade_interp)) + "</h1><div class=\"desc\">" + (jade.escape(null == (jade_interp = house['property information']) ? "" : jade_interp)) + "</div><nav class=\"house-tab\">");
var summary='', gallery='', neighbor=''
var loc = window.location.pathname.split('/')
var loc_path = loc[2]
if (loc.length==3) summary='active'
if (loc_path=='gallery') gallery='active'
if (loc_path=='neighbor') neighbor='active'
buf.push("<a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + (jade.cls([summary], [true])) + ">" + (jade.escape(null == (jade_interp = _('summary')) ? "" : jade_interp)) + "</a><a" + (jade.attr("href", "/house/gallery/" + (house._id) + "", true, false)) + (jade.cls([gallery], [true])) + ">" + (jade.escape(null == (jade_interp = _('gallery')) ? "" : jade_interp)) + "</a></nav></div><div class=\"gallery-boxs\">");
// iterate house.gallery
;(function(){
  var $$obj = house.gallery;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id) + "/480x325", true, false)) + (jade.attr("id", photo._id, true, false)) + "/>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id) + "/480x325", true, false)) + (jade.attr("id", photo._id, true, false)) + "/>");
    }

  }
}).call(this);

buf.push("</div></div>");}("house" in locals_for_with?locals_for_with.house:typeof house!=="undefined"?house:undefined,"window" in locals_for_with?locals_for_with.window:typeof window!=="undefined"?window:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined));;return buf.join("");
}, "house_list": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (active_state, state_abbr, active_sort, _, active_order, house_list, sanitize, agent_list, article_list) {
buf.push("<div class=\"house-list\"><div class=\"house-filter\"><div class=\"hf-state\">");
var cls = active_state ? '':' active'
buf.push("<a data-state=\"\"" + (jade.cls([cls], [true])) + ">全境:</a>");
// iterate state_abbr
;(function(){
  var $$obj = state_abbr;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var s = $$obj[$index];

var cls = (s[1] == active_state) ? 'active':''
buf.push("<a" + (jade.attr("data-state", s[1], true, false)) + (jade.cls([cls], [true])) + ">" + (jade.escape(null == (jade_interp = s[0]) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var s = $$obj[$index];

var cls = (s[1] == active_state) ? 'active':''
buf.push("<a" + (jade.attr("data-state", s[1], true, false)) + (jade.cls([cls], [true])) + ">" + (jade.escape(null == (jade_interp = s[0]) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div><div class=\"hf-order\"><label>排序方式:</label>");
// iterate ['create_time', 'size', 'price']
;(function(){
  var $$obj = ['create_time', 'size', 'price'];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var v = $$obj[$index];

var cls = (v == active_sort) ? 'active':''
buf.push("<a" + (jade.attr("data-sort", v, true, false)) + (jade.cls([cls], [true])) + ">" + (jade.escape(null == (jade_interp = _(v)) ? "" : jade_interp)));
if (v == active_sort)
{
buf.push("<i" + (jade.attr("data-dir", active_order, true, false)) + "></i>");
}
else
{
buf.push("<i></i>");
}
buf.push("</a>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var v = $$obj[$index];

var cls = (v == active_sort) ? 'active':''
buf.push("<a" + (jade.attr("data-sort", v, true, false)) + (jade.cls([cls], [true])) + ">" + (jade.escape(null == (jade_interp = _(v)) ? "" : jade_interp)));
if (v == active_sort)
{
buf.push("<i" + (jade.attr("data-dir", active_order, true, false)) + "></i>");
}
else
{
buf.push("<i></i>");
}
buf.push("</a>");
    }

  }
}).call(this);

buf.push("</div></div>");
// iterate house_list
;(function(){
  var $$obj = house_list;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var house = $$obj[idx];

buf.push("<div class=\"house\"><div class=\"gallery\">");
if ( house.gallery)
{
buf.push("<img" + (jade.attr("src", "/assets/" + (house.gallery[0]._id) + "/335x320", true, false)) + " class=\"cover\"/><div class=\"thumb\">");
var size= '/85x80'
var i = 0, s = size || '/80x56'
// iterate house.gallery
;(function(){
  var $$obj = house.gallery;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id+s) + "", true, false)) + (jade.cls([(i==0)?"active":''], [true])) + "/>");
i++
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id+s) + "", true, false)) + (jade.cls([(i==0)?"active":''], [true])) + "/>");
i++
    }

  }
}).call(this);

buf.push("</div>");
}
else
{
buf.push("<p>gallery coming soon...</p>");
}
buf.push("</div><h2 class=\"hd\"><a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = house.title || 'N/A') ? "" : jade_interp)) + "</a></h2><div class=\"bd\"><p>" + (jade.escape(null == (jade_interp = house.address.en) ? "" : jade_interp)) + "</p>");
var hide = 1
buf.push("<div class=\"house-summary\"><p class=\"grey\"><span>" + (jade.escape((jade_interp = _('price')) == null ? '' : jade_interp)) + ": </span><em class=\"price\">" + (jade.escape((jade_interp = house.price.cn || 'N/A') == null ? '' : jade_interp)) + "</em>美元</p><p><span class=\"grey\">" + (jade.escape((jade_interp = _('size')) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house['square metre'] || 'N/A') == null ? '' : jade_interp)) + "平方米</p><p><span class=\"grey\">" + (jade.escape((jade_interp = _('construction year')) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house['construction year'] || 'N/A') == null ? '' : jade_interp)) + "年</p><ul class=\"compact\">");
var fields = hide ? ['bedroom', 'bathroom'] : ['floor', 'room', 'bedroom', 'bathroom']
// iterate fields
;(function(){
  var $$obj = fields;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var f = $$obj[$index];

buf.push("<li><span class=\"grey\">" + (jade.escape((jade_interp = _(f)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[f] || 'N/A') == null ? '' : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var f = $$obj[$index];

buf.push("<li><span class=\"grey\">" + (jade.escape((jade_interp = _(f)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[f] || 'N/A') == null ? '' : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div><p class=\"desc\">" + (jade.escape(null == (jade_interp = sanitize(house['property information'], 75)) ? "" : jade_interp)) + "</p></div></div>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var house = $$obj[idx];

buf.push("<div class=\"house\"><div class=\"gallery\">");
if ( house.gallery)
{
buf.push("<img" + (jade.attr("src", "/assets/" + (house.gallery[0]._id) + "/335x320", true, false)) + " class=\"cover\"/><div class=\"thumb\">");
var size= '/85x80'
var i = 0, s = size || '/80x56'
// iterate house.gallery
;(function(){
  var $$obj = house.gallery;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id+s) + "", true, false)) + (jade.cls([(i==0)?"active":''], [true])) + "/>");
i++
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<img" + (jade.attr("src", "/assets/" + (photo._id+s) + "", true, false)) + (jade.cls([(i==0)?"active":''], [true])) + "/>");
i++
    }

  }
}).call(this);

buf.push("</div>");
}
else
{
buf.push("<p>gallery coming soon...</p>");
}
buf.push("</div><h2 class=\"hd\"><a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = house.title || 'N/A') ? "" : jade_interp)) + "</a></h2><div class=\"bd\"><p>" + (jade.escape(null == (jade_interp = house.address.en) ? "" : jade_interp)) + "</p>");
var hide = 1
buf.push("<div class=\"house-summary\"><p class=\"grey\"><span>" + (jade.escape((jade_interp = _('price')) == null ? '' : jade_interp)) + ": </span><em class=\"price\">" + (jade.escape((jade_interp = house.price.cn || 'N/A') == null ? '' : jade_interp)) + "</em>美元</p><p><span class=\"grey\">" + (jade.escape((jade_interp = _('size')) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house['square metre'] || 'N/A') == null ? '' : jade_interp)) + "平方米</p><p><span class=\"grey\">" + (jade.escape((jade_interp = _('construction year')) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house['construction year'] || 'N/A') == null ? '' : jade_interp)) + "年</p><ul class=\"compact\">");
var fields = hide ? ['bedroom', 'bathroom'] : ['floor', 'room', 'bedroom', 'bathroom']
// iterate fields
;(function(){
  var $$obj = fields;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var f = $$obj[$index];

buf.push("<li><span class=\"grey\">" + (jade.escape((jade_interp = _(f)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[f] || 'N/A') == null ? '' : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var f = $$obj[$index];

buf.push("<li><span class=\"grey\">" + (jade.escape((jade_interp = _(f)) == null ? '' : jade_interp)) + ":</span>" + (jade.escape((jade_interp = house[f] || 'N/A') == null ? '' : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div><p class=\"desc\">" + (jade.escape(null == (jade_interp = sanitize(house['property information'], 75)) ? "" : jade_interp)) + "</p></div></div>");
    }

  }
}).call(this);

buf.push("<div class=\"pager\"></div></div><div class=\"house-list-side\"><div class=\"box relative-agent\"><h2 class=\"hd\">" + (jade.escape(null == (jade_interp = _('relative agent')) ? "" : jade_interp)) + "</h2><div class=\"bd\">");
// iterate agent_list
;(function(){
  var $$obj = agent_list;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var agent = $$obj[$index];

if ( agent)
{
buf.push("<div class=\"agent-photo\"><img" + (jade.attr("src", "/assets/" + (agent.photo[0]) + "/100x100", true, false)) + "/><p>" + (jade.escape((jade_interp = agent.name) == null ? '' : jade_interp)) + " [" + (jade.escape((jade_interp = agent.title) == null ? '' : jade_interp)) + "]</p></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var agent = $$obj[$index];

if ( agent)
{
buf.push("<div class=\"agent-photo\"><img" + (jade.attr("src", "/assets/" + (agent.photo[0]) + "/100x100", true, false)) + "/><p>" + (jade.escape((jade_interp = agent.name) == null ? '' : jade_interp)) + " [" + (jade.escape((jade_interp = agent.title) == null ? '' : jade_interp)) + "]</p></div>");
}
    }

  }
}).call(this);

buf.push("</div></div><div class=\"box relative-article\"><h2 class=\"hd\">" + (jade.escape(null == (jade_interp = _('relative article')) ? "" : jade_interp)) + "</h2><div class=\"bd\">");
// iterate article_list
;(function(){
  var $$obj = article_list;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var article = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/article/" + (article._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = article.title) ? "" : jade_interp)) + "</a><div class=\"abbr\">" + (jade.escape(null == (jade_interp = sanitize(article.content, 100)) ? "" : jade_interp)) + "</div>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var article = $$obj[idx];

buf.push("<a" + (jade.attr("href", "/article/" + (article._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = article.title) ? "" : jade_interp)) + "</a><div class=\"abbr\">" + (jade.escape(null == (jade_interp = sanitize(article.content, 100)) ? "" : jade_interp)) + "</div>");
    }

  }
}).call(this);

buf.push("</div></div></div>");}("active_state" in locals_for_with?locals_for_with.active_state:typeof active_state!=="undefined"?active_state:undefined,"state_abbr" in locals_for_with?locals_for_with.state_abbr:typeof state_abbr!=="undefined"?state_abbr:undefined,"active_sort" in locals_for_with?locals_for_with.active_sort:typeof active_sort!=="undefined"?active_sort:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined,"active_order" in locals_for_with?locals_for_with.active_order:typeof active_order!=="undefined"?active_order:undefined,"house_list" in locals_for_with?locals_for_with.house_list:typeof house_list!=="undefined"?house_list:undefined,"sanitize" in locals_for_with?locals_for_with.sanitize:typeof sanitize!=="undefined"?sanitize:undefined,"agent_list" in locals_for_with?locals_for_with.agent_list:typeof agent_list!=="undefined"?agent_list:undefined,"article_list" in locals_for_with?locals_for_with.article_list:typeof article_list!=="undefined"?article_list:undefined));;return buf.join("");
}, "house_neighbor": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (house, window, _) {
buf.push("<div class=\"house-single house-neighbor\"><div class=\"house-main\"><h1 class=\"hd\">" + (jade.escape(null == (jade_interp = house.address.en) ? "" : jade_interp)) + "</h1><div class=\"desc\">" + (jade.escape(null == (jade_interp = house['property information']) ? "" : jade_interp)) + "</div><nav class=\"house-tab\">");
var summary='', gallery='', neighbor=''
var loc = window.location.pathname.split('/')
var loc_path = loc[2]
if (loc.length==3) summary='active'
if (loc_path=='gallery') gallery='active'
if (loc_path=='neighbor') neighbor='active'
buf.push("<a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + (jade.cls([summary], [true])) + ">" + (jade.escape(null == (jade_interp = _('summary')) ? "" : jade_interp)) + "</a><a" + (jade.attr("href", "/house/gallery/" + (house._id) + "", true, false)) + (jade.cls([gallery], [true])) + ">" + (jade.escape(null == (jade_interp = _('gallery')) ? "" : jade_interp)) + "</a></nav></div><div class=\"neighbor-boxs\">");
// iterate house.neighbor
;(function(){
  var $$obj = house.neighbor;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var near = $$obj[$index];

buf.push("<a" + (jade.attr("href", "/house/" + (near._id) + "", true, false)) + "><img" + (jade.attr("src", "/assets/" + (near.photo._id) + "/480x325", true, false)) + "/><p>" + (jade.escape(null == (jade_interp = near.address.en) ? "" : jade_interp)) + "</p></a>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var near = $$obj[$index];

buf.push("<a" + (jade.attr("href", "/house/" + (near._id) + "", true, false)) + "><img" + (jade.attr("src", "/assets/" + (near.photo._id) + "/480x325", true, false)) + "/><p>" + (jade.escape(null == (jade_interp = near.address.en) ? "" : jade_interp)) + "</p></a>");
    }

  }
}).call(this);

buf.push("</div></div>");}("house" in locals_for_with?locals_for_with.house:typeof house!=="undefined"?house:undefined,"window" in locals_for_with?locals_for_with.window:typeof window!=="undefined"?window:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined));;return buf.join("");
}, "index": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (state_list, _, house_list) {
buf.push("<div class=\"index-slider\"><ul id=\"carousel\">");
var i=1
// iterate ['/search?t=house&state=CA', '/search?t=house&q=%E6%9B%BC%E5%93%88%E9%A1%BF', '/search?t=house&q=%E9%95%BF%E5%B2%9B']
;(function(){
  var $$obj = ['/search?t=house&state=CA', '/search?t=house&q=%E6%9B%BC%E5%93%88%E9%A1%BF', '/search?t=house&q=%E9%95%BF%E5%B2%9B'];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var url = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", url, true, false)) + "><img" + (jade.attr("src", "/static/img/banner" + (i) + ".jpg", true, false)) + "/></a></li>");
i++
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var url = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", url, true, false)) + "><img" + (jade.attr("src", "/static/img/banner" + (i) + ".jpg", true, false)) + "/></a></li>");
i++
    }

  }
}).call(this);

buf.push("</ul></div><div class=\"index-state\">");
// iterate state_list
;(function(){
  var $$obj = state_list;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var s = $$obj[$index];

var large = s.total >1 ?'state-large': ''
buf.push("<div" + (jade.cls(["state " + (large) + ""], [true])) + "><a" + (jade.attr("href", "/search?t=house&state=" + (s._id.abbr) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = s._id.cn) ? "" : jade_interp)) + "</a><span class=\"grey\">" + (jade.escape((jade_interp = s.total) == null ? '' : jade_interp)) + "条</span></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var s = $$obj[$index];

var large = s.total >1 ?'state-large': ''
buf.push("<div" + (jade.cls(["state " + (large) + ""], [true])) + "><a" + (jade.attr("href", "/search?t=house&state=" + (s._id.abbr) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = s._id.cn) ? "" : jade_interp)) + "</a><span class=\"grey\">" + (jade.escape((jade_interp = s.total) == null ? '' : jade_interp)) + "条</span></div>");
    }

  }
}).call(this);

buf.push("</div><div class=\"index-intro\"><div class=\"hd\">" + (jade.escape(null == (jade_interp = _('intro title')) ? "" : jade_interp)) + "</div><div class=\"bd\"><div class=\"item\"><img src=\"/static/img/intro.1.png\"/><p>透明美国房产价格信息</p></div><div class=\"item\"><img src=\"/static/img/intro.2.png\"/><p>选房买房置业专家向导</p></div><div class=\"item\"><img src=\"/static/img/intro.3.png\"/><p>人性化知识库答疑解忧</p></div></div></div><div class=\"index-house\"><div class=\"hd\">" + (jade.escape(null == (jade_interp = _('editor choice')) ? "" : jade_interp)) + "</div>");
// iterate house_list
;(function(){
  var $$obj = house_list;
  if ('number' == typeof $$obj.length) {

    for (var idx = 0, $$l = $$obj.length; idx < $$l; idx++) {
      var house = $$obj[idx];

buf.push("<div class=\"house\"><div class=\"gallery\">");
if ( house.gallery)
{
buf.push("<img" + (jade.attr("src", "/assets/" + (house.gallery[0]._id) + "/235x250", true, false)) + " class=\"cover\"/>");
}
buf.push("</div><h2 class=\"hd\"><a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = house.title || 'N/A') ? "" : jade_interp)) + "</a></h2><div class=\"bd\"><p class=\"desc\">" + (jade.escape(null == (jade_interp = house['property information'] ? house['property information'].replace(/(<([^>]+)>)/ig,"").substring(0,75) : 'N/A') ? "" : jade_interp)) + "</p><p class=\"more\"><a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + ">查看详情</a></p></div></div>");
    }

  } else {
    var $$l = 0;
    for (var idx in $$obj) {
      $$l++;      var house = $$obj[idx];

buf.push("<div class=\"house\"><div class=\"gallery\">");
if ( house.gallery)
{
buf.push("<img" + (jade.attr("src", "/assets/" + (house.gallery[0]._id) + "/235x250", true, false)) + " class=\"cover\"/>");
}
buf.push("</div><h2 class=\"hd\"><a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + ">" + (jade.escape(null == (jade_interp = house.title || 'N/A') ? "" : jade_interp)) + "</a></h2><div class=\"bd\"><p class=\"desc\">" + (jade.escape(null == (jade_interp = house['property information'] ? house['property information'].replace(/(<([^>]+)>)/ig,"").substring(0,75) : 'N/A') ? "" : jade_interp)) + "</p><p class=\"more\"><a" + (jade.attr("href", "/house/" + (house._id) + "", true, false)) + ">查看详情</a></p></div></div>");
    }

  }
}).call(this);

buf.push("</div>");}("state_list" in locals_for_with?locals_for_with.state_list:typeof state_list!=="undefined"?state_list:undefined,"_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined,"house_list" in locals_for_with?locals_for_with.house_list:typeof house_list!=="undefined"?house_list:undefined));;return buf.join("");
}, "login": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (_) {
buf.push("<form id=\"user-form\" method=\"POST\" action=\"/api/login\"><div class=\"field\"><label>" + (jade.escape(null == (jade_interp = _('email')) ? "" : jade_interp)) + "</label><input type=\"email\" name=\"email\"/></div><div class=\"field\"><label>" + (jade.escape(null == (jade_interp = _('password')) ? "" : jade_interp)) + "</label><input type=\"password\" name=\"password\"/></div><div class=\"action\"><button id=\"submit\">" + (jade.escape(null == (jade_interp = _('login')) ? "" : jade_interp)) + "</button></div></form>");}("_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined));;return buf.join("");
}, "register": function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (_) {
buf.push("<form id=\"user-form\" method=\"POST\" action=\"/api/register\" class=\"register\"><div class=\"field\"><label>" + (jade.escape(null == (jade_interp = _('email')) ? "" : jade_interp)) + "</label><input type=\"email\" name=\"email\"" + (jade.attr("placeholder", _('email is your login username'), true, false)) + "/></div><div class=\"field\"><label>" + (jade.escape(null == (jade_interp = _('phone number')) ? "" : jade_interp)) + "</label><input type=\"tel\" name=\"phone\"/></div><div class=\"field\"><label>" + (jade.escape(null == (jade_interp = _('password')) ? "" : jade_interp)) + "</label><input type=\"password\" name=\"password\"/></div><div class=\"field\"><label>" + (jade.escape(null == (jade_interp = _('retry password')) ? "" : jade_interp)) + "</label><input type=\"password\" name=\"retry\"/></div><div class=\"inline field\"><input type=\"checkbox\" name=\"term\" id=\"term\"/><label for=\"term\">" + (jade.escape(null == (jade_interp = _('I agree to the')) ? "" : jade_interp)) + "<a href=\"/terms\">" + (jade.escape(null == (jade_interp = _('terms and conditions')) ? "" : jade_interp)) + "</a></label></div><div class=\"action\"><button id=\"submit\" type=\"submit\">" + (jade.escape(null == (jade_interp = _('submit')) ? "" : jade_interp)) + "</button><a href=\"/login\" class=\"ui button\">" + (jade.escape(null == (jade_interp = _('already have account?')) ? "" : jade_interp)) + "</a></div></form>");}("_" in locals_for_with?locals_for_with._:typeof _!=="undefined"?_:undefined));;return buf.join("");
}};