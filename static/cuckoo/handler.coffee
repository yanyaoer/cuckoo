B = require('backbone')
M = require('backbone.marionette')
B.$ = M.$ = $ = window.jQuery = require('jquery')
cookie = require('cookie')
qs = require('query-string')
carousel = require('carousel')
U = require('./util')
tpl = require('./tpl')


module.exports =
  register: ()->
    $('#contain').html tpl.register()
    $('#user-form [type="email"]').blur ->
      el = $(this)
      msg = (msg)->
        el.parent().find('.msg').remove()
        el.parent().append("<p class='msg'>#{msg}</p>")
      if el.val().match(/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/i)
        $.get "/api/check?mail=#{el.val()}", (d)->
          msg d.msg
      else
        msg "邮箱格式错误"
    $('#user-form').submit (e)->
      e.preventDefault()
      el = $(this)
      d =
        email: el.find('[name=email]').val()
        phone: el.find('[name=phone]').val()
        password: el.find('[name=password]').val()
      if not el.find('#term:checked')[0]
        return U.tip('please read and agree the terms')
      if el.find('[name=retry]').val() isnt d.password
        return U.tip('password dose not match')
      $.ajax
        type: el.attr('method')
        url: el.attr('action')
        data: d
      .done (res)->
        $('#user-form').append('<p class="msg-ok">注册成功，<em class="reg_timer">5</em>s后自动 <a href="/">返回首页</a></p>')
        setInterval ->
          timer = $('.reg_timer').text()
          if timer == '0'
            window.location.href = '/'
          else
            $('.reg_timer').text timer-1
        ,1000

  login: ()->
    $('#contain').html tpl.login()
    $('#user-form').submit (e)->
      e.preventDefault()
      el = $(this)
      $.ajax
        type: el.attr('method')
        url: el.attr('action')
        data:
          email: el.find('[name=email]').val()
          password: el.find('[name=password]').val()
      .done (res)->
        if window.location.search
          window.location.href=decodeURIComponent(window.location.search.split('?next=')[1])
        else
          window.location.href='/'

  logout: ()->
    cookie('user', null)
    window.location.href='/'

  search: ()->
    q = window.location.search
    arg = qs.parse(q)
    $.getJSON "/api/search#{q}", (d)->
      #console.log d
      if arg.t == 'article'
        t = tpl.article_list
        model = new B.Model d
      else
        t = tpl.house_list
        d.state_abbr = U.state_abbr
        d.active_sort = arg.sort || ''
        d.active_state = arg.state || ''
        d.active_order = arg.order || ''
        model = new B.Model d
      view = M.ItemView.extend
        template: t
        model: model
      cuckoo.contain.show(new view())
      U.pagination(d.page_max)

      if arg.t == 'house'
        U.house_list(d, arg)

  article_single: (_id)->
    $.getJSON "/api/article/#{_id}", (d)->
      view = M.ItemView.extend
        template: tpl.article_single
        model: new B.Model
          article: d
      cuckoo.contain.show(new view())

  article_list: ()->
    $.getJSON "/api/article/", (d)->
      view = M.ItemView.extend
        template: tpl.article_list
        model: new B.Model
          article_list: d
      cuckoo.contain.show(new view())

  house: (query)->
    #console.log 'get house ' + query
    $.getJSON "/api/house/#{query}", (d)->
      view = M.ItemView.extend
        template: tpl.house
        model: new B.Model
          house: d
      cuckoo.contain.show(new view())

      thumb = $('.thumb img')
      inner = thumb.parent()
      U.switch $('#cover'), thumb
      U.map($('#map')[0], d)

      min = 5
      if thumb.length < min
        return
      max = min-thumb.length-1
      idx = 0
      $('.slider i').click (e)->
        if (e.target.className == 'move-right' && idx > max)
          idx--
        if (e.target.className == 'move-left' && idx < 1)
          idx++
        inner.css('margin-left': idx*85)

  house_gallery: (query)->
    #console.log 'get house ' + query
    $.getJSON "/api/house/#{query}", (d)->
      view = M.ItemView.extend
        template: tpl.house_gallery
        model: new B.Model
          house: d
      cuckoo.contain.show(new view())
      U.gallery()

  house_neighbor: (query)->
    #console.log 'get house ' + query
    $.getJSON "/api/house/#{query}", (d)->
      view = M.ItemView.extend
        template: tpl.house_neighbor
        model: new B.Model
          house: d
      cuckoo.contain.show(new view())

  house_list: ()->
    $.getJSON "/api/house/", (d)->
      view = M.ItemView.extend
        template: tpl.house_list
        model: new B.Model d
      cuckoo.contain.show(new view())
      U.house_list()

  index: ()->
    $.getJSON "/api/", (d)->
      view = M.ItemView.extend
        template: tpl.index
        model: new B.Model d
      cuckoo.contain.show(new view())

      new carousel('carousel').start(0,5000)
      $('#carousel a').click (e)->
        ev = ['_trackEvent', 'banner', 'click', $(this).attr('href')]
        ga 'send', 'event', 'banner', 'click', $(this).attr('href')
        window._hmt.push ev if window._hmt

  default: ()->
    #console.log 'default'
    view = M.ItemView.extend
      template: tpl['404']
      model: new B.Model
    cuckoo.contain.show(new view())
