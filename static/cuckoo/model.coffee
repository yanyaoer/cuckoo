house =
  'address':''
  'price':''
  'property information':'textarea'
  '_main':
    '_size':
      'bedrooms':''
      'bathrooms':''
      'garage':''
      'square ft':''
      'square metre':''
      'lot size':''
    '_value':
      'price per sq/ft for this property':''
      'average price per sq/ft for this area':''
      'median home price':''
    '_extra':
      'year built':''
      'school district':''
      'china town':''
      'university place':''
      'mls number':''
      'live date':''
      'property type':''
      'area location':''
      'subdivision':''
      'county':''
      'status':''
      'map page':''
  'general':
    'rooms':''
    'stories':''
    'bedrooms':''
    'full bathrooms':''
    'garage capacity':''
    'parking description':''
    'fireplaces':''
    'square footage':''
  'exterior comments':
    'exterior features':''
    'fence':''
    'fence description':''
    'pool':''
  'interior comments':
    'dining room level':''
    'flooring type':''
    'interior features':''
    'kitchen level':''
    'living room level':''
    'rooms other':''
  'construction':
    'exterior siding':''
    'foundation type':''
    'new construction':''
    'style':''
    'year built':''
  'equipment':
    'appliances':''
    'cooling':''
    'heating':''
    'equipment':''
    'water heater type':''
    'sewer type':''
    'water type':''
  'property':
    'listing agent':''
    'listing provided courtesy of':''
    'mls number':''
  'lot comments':
    'view description':''
    'waterfront description':''
  'additional information':
    'high school':''
    'tax amount':''


module.exports =
  house: house
  test_house: {'车库容量': '2', '客厅楼层': '1', '始售时间': '2013年7月3号', '房产信息': '优雅的独户住房，全新家装，适合大家庭居住和聚会娱乐，大型带就餐面积的厨房（Eat in Kitchen, EIK）,  日光浴室（Sunroom），家庭影院 。另有的独立房屋里带有卧室和浴室，和小厨房，有与主屋连接的专用出入道。房产附近有高评价的学校，很棒的家庭亲子区域，以及独立存储室。', '卧房数': '5', '水加热系统': '电', '地段': 'North Central Virginia Beach北中佛吉尼亚滩', '停车': '2车位，连体车库，可多车', '销售代理': 'Gail Gowin', '地块面积': '未知', '泳池': '无', '上市信息提供': 'Prudential Towne Realty', '水边地': '否', '郡': '佛吉尼亚滩', '浴室／卫生 间（间）': '3', '厨房楼层': '1', '建筑年份': '1978', '其他设施': '阁楼电扇，有线电视接入口，屋顶电扇，车库开门机，报警系统', '制热': '热泵', '排水类型': '市／郡', '壁炉数': '1', '围栏': '无', '房间数': '12', '外墙': '砖，乙烯', 'MLS号码': '1327900', '平方米': '397', '居住房中间价': '$269,900', '车库': '2', '地址': '808 Coverdale Lane, Virginia Beach, VA 23452', '本地区平方英尺均价': '$164', '所属区': 'Royal Grant', '楼层数': '2', '高中': 'First Colonial', '餐厅楼层层': '1', '房产外部设施': '露台，草坪地内洒水器，水泵，水井，木制', '地产类型': '独户住房', '其他屋内设施': '楼阁，早餐区，车库上成品间，前厅，客人套间，带浴室主卧', '平方英尺单价': '$169', '销售情况': '在售', '建筑风格': '传统型', '完整浴室': '3', '内部设施': '煤气壁炉，下拉式阁楼楼梯，步入室阁楼，步入式衣帽间，窗户装饰窗帘，残障设施', '卧室（ 间）': '5', '景观': '林木', '学区': 'n/a', '地基类型': '槽隙（Crawl）', '家用电器': '220伏电压，洗碗机，废物处理，微波炉，电灶，冰箱', '地面材料': '地毯，瓷砖，漆涂层', '供水类型': '市／郡', '制冷': '热泵', '地税': '$5350', '新建屋': '否', '价格': '$72万5千', 'MLS号': '1327990', '平方英尺': '4269'}
