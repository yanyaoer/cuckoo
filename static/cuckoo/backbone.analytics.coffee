((factory) ->
  "use strict"
  if typeof define is "function" and define.amd
    define [ "backbone" ], factory
  else if typeof exports is "object"
    module.exports = factory(require("backbone"))
  else
    factory window.Backbone
) (Backbone) ->
  "use strict"

  ((i,s,o,g,r,a,m)->
    i['GoogleAnalyticsObject']=r
    i[r]=i[r]||()->
      (i[r].q=i[r].q||[]).push(arguments)
    i[r].l=1*new Date()
    a=s.createElement(o)
    m=s.getElementsByTagName(o)[0]
    a.async=1
    a.src=g
    m.parentNode.insertBefore(a,m)
  )(window,document,'script','//www.google-analytics.com/analytics.js','ga')

  window._hmt = []
  (->
    hm = document.createElement("script")
    hm.src = "//hm.baidu.com/hm.js?6e18f9ee8c1a3a4fc0f10e796d8414c4"
    s = document.getElementsByTagName("script")[0]
    s.parentNode.insertBefore(hm, s)
  )()

  ga('create', 'UA-52545881-1', 'auto')

  loadUrl = Backbone.History::loadUrl
  Backbone.History::loadUrl = (fragmentOverride) ->
    matched = loadUrl.apply(this, arguments)
    gaFragment = @fragment
    gaFragment = "/" + gaFragment  unless /^\//.test(gaFragment)
    
    # legacy version
    window._gaq.push [ "_trackPageview", gaFragment ] if typeof window._gaq isnt "undefined"
    window._hmt.push [ "_trackPageview", gaFragment ]
    
    # Analytics.js
    ga = undefined
    if window.GoogleAnalyticsObject and window.GoogleAnalyticsObject isnt "ga"
      ga = window.GoogleAnalyticsObject
    else
      ga = window.ga
    ga "send", "pageview", gaFragment  if typeof ga isnt "undefined"
    matched
