$ = require('jquery')
qs = require('query-string')
pager = require('pager')

head = document.getElementsByTagName( "head" )[0]

_load = (url, fn)->
  script = document.createElement('script')
  script.async = true
  script.src = url
  head.insertBefore(script, head.firstChild)
  script.onloadDone = false
  if fn
    script.onload = ()->
      setTimeout(fn, 1000)

window.sanitize = (str, len)->
  return if str then str.replace(/(<([^>]+)>)/ig,"").substring(0, len) else ''

util =
  _insertIMG: (img)->
    s = window.getSelection()
    n = s.baseNode
    c = $('#editor .ed-content')
    if $.contains(c[0], s)
      s.getRangeAt(0).insertNode(img)
    else
      c.append img

  tip: (str)->
    el = $("<div class='ui tip small red message'>#{str}</div>")
    $(document.body).append el
    el.fadeOut 2000, ()->
      el.remove()

  map: (el,d)->
    key = "AheyINv5pZOZgYhxGYmMttiF106XCbuWodZf_DMD5x-SWQl2OzAfywT_9wCYCZa4"
    loc = null

    window.geo_cb = (res)->
      list = res.resourceSets[0].resources[0].point.coordinates
      loc = new Microsoft.Maps.Location(list[0], list[1])
      _map()

    _jsonp = ()->
      _load "http://dev.virtualearth.net/REST/v1/Locations" +
            "?query=#{encodeURI(d.address.en)}&output=json&jsonp=geo_cb&key=#{key}"

    _map = ()->
      conf =
        credentials: key
        center: loc
        zoom: 10
      m = new Microsoft.Maps.Map(el, conf)
      pin = new Microsoft.Maps.Pushpin(m.getCenter(), {text: '1'})
      m.entities.push pin

    _render = ()->
      if d.geolocation
        list = d.geolocation
        loc = new Microsoft.Maps.Location(list[0], list[1])
        _map()
      else
        _jsonp()

    if window.Microsoft
      _render()
    else
      _load 'https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&mkt=zh-cn', _render

  switch: (target, trigger)->
    trigger.click (e)->
      el = e.target
      if el.className != 'active'
        trigger.removeClass('active')
        uri = el.getAttribute('src').replace('/80x56', '/490x325')
        target.attr('src', uri)
        el.className = 'active'

  pagination: (total)->
    arg = qs.parse(window.location.search)
    p = pager(document.querySelector('.pager')).total(total).render()
    sp = arg.p || 1
    p.select(sp-1)
    p.on 'show', (n)->
      arg.p=n+1
      R.navigate "/search?#{qs.stringify(arg)}", {trigger: true}
      window.scrollTo(0, 0)

  house_list: ()->

    $('.thumb').click (e)->
      el = e.target
      if el.className != 'active'
        trigger = $(this)
        trigger.find('img').removeClass('active')
        el.className = 'active'
        uri = el.getAttribute('src').replace('/85x80', '/335x320')
        trigger.prev().attr('src', uri)

    # filter
    $('.hf-order a').click (e)->
      el = e.target
      arg = qs.parse(window.location.search)
      arg.p = 1
      arg.sort = $(el).data('sort')
      if arg['order'] && arg['order'] != '0'
        arg.order = '0'
      else
        arg.order = '1'
      R.navigate "/search?#{qs.stringify(arg)}", {trigger: true}
      window.scrollTo(0,0)

    $('.hf-state a').click (e)->
      el = e.target
      s = $(el).data('state')
      arg = qs.parse(window.location.search)
      arg.p = 1
      arg.state = if (arg.state == s) then '' else s
      R.navigate "/search?#{qs.stringify(arg)}", {trigger: true}
      window.scrollTo(0,0)

  gallery: ()->
    img_list = $('.gallery-boxs img')
    img_list.click (e)->
      img = $(this)
      src = img.attr('src')
      size = '/480x325'
      s = if (src.indexOf(size) == -1) then src+size else src.replace(size, '')
      #console.log src,size, s, (src.indexOf(size) == -1)
      #return
      hash = window.location.hash
      img.toggleClass('active')
        .attr('src', s)
      fn = ()->
        if hash
          window.location.hash = ''
        else
          window.location.hash = img.attr('id')
      setTimeout fn, 1100


  search: ()->
    label = $('#search-label label')
    type = $('#search-type')

    label.click (e)->
      el = $(this)
      label.parent().find('.active').removeClass('active')
      el.addClass('active')
      type.attr('value', el.attr('for'))

    $('.search').submit (e)->
      e.preventDefault()
      d = $(this).serialize()
      R.navigate "/search?#{d}", {trigger: true}
      window.scrollTo(0,0)

  state_abbr: [
    ['阿拉巴马', 'AL'],
    ['阿拉斯加', 'AK'],
    ['阿利桑那', 'AZ'],
    ['阿肯色', 'AR'],
    ['加利福尼亚', 'CA'],
    ['科罗拉多', 'CO'],
    ['康涅狄格', 'CT'],
    ['特拉华', 'DE'],
    ['佛罗里达', 'FL'],
    ['乔治亚', 'GA'],
    ['夏威夷', 'HI'],
    ['爱达荷', 'ID'],
    ['伊利诺斯', 'IL'],
    ['印第安纳', 'IN'],
    ['爱荷华', 'IA'],
    ['堪萨斯', 'KS'],
    ['肯塔基', 'KY'],
    ['路易斯安那', 'LA'],
    ['缅因', 'ME'],
    ['马里兰', 'MD'],
    ['马萨诸塞', 'MA'],
    ['密歇根', 'MI'],
    ['明尼苏达', 'MN'],
    ['密西西比', 'MS'],
    ['密苏里', 'MO'],
    ['蒙大拿', 'MT'],
    ['内布拉斯加', 'NE'],
    ['内华达', 'NV'],
    ['新罕布什尔', 'NH'],
    ['新泽西', 'NJ'],
    ['新墨西哥', 'NM'],
    ['纽约', 'NY'],
    ['北卡罗来纳', 'NC'],
    ['北达科他', 'ND'],
    ['俄亥俄', 'OH'],
    ['俄克拉荷马', 'OK'],
    ['俄勒冈', 'OR'],
    ['宾夕法尼亚', 'PA'],
    ['罗得岛', 'RI'],
    ['南卡罗来纳', 'SC'],
    ['南达科他', 'SD'],
    ['田纳西', 'TN'],
    ['得克萨斯', 'TX'],
    ['犹他', 'UT'],
    ['佛蒙特', 'VT'],
    ['弗吉尼亚', 'VA'],
    ['华盛顿', 'WA'],
    ['西弗吉尼亚', 'WV'],
    ['威斯康辛', 'WI'],
    ['怀俄明', 'WY']
  ]

module.exports = util
