from bson.dbref import DBRef

def gen_field(handler, tuple=(), prefix='', house={}):
  tpl = {
    'property information': '<textarea name="{0}" placeholder="{1}">{2}</textarea>',
    'geolocation': '''<div class="ui icon input">
        <input type="text" name="{0}" placeholder="{1}" value="{2}" />
        <div class="ui teal right icon button" id="update-geo">
          <i class="flag icon"></i>
        </div>
      </div>''',
    'default': '<input type="text" name="{0}" placeholder="{1}" value="{2}" />',
  }
  def process(item):
    n = type(item).__name__
    if n == 'tuple':
      return gen_field(handler, item, house=house)
    elif n == 'dict':
      if 'state' in item:
        key = 'state'
        title = handler.locale.translate(key)
        obj = house.get(key, {})
        val = "{cn}-{en}-{abbr}".format(**obj) if obj else '' 
        return """<h3>{0}</h3>
        <input type="hidden" name="state" value="{1}" />
        <div class='ui state-select' data-val='{1}'></div>""".format(title, val)
      return ''.join(['<h3>{0}</h3>{1}'.format(handler.locale.translate(k),
        gen_field(handler, v, prefix=k, house=house)) for k,v in item.items()])
    elif n == 'str':
      val = house.get(prefix, {}).get(item, '') if prefix else house.get(item, '')
      name = prefix+'.'+item if prefix else item
      label =  {
        "refrigerator": "cooling",
        "warmer": "heating",
        "water warmer": "fuel type",
      }.get(name, name)
      if not val:
        val = ''
      if item == 'geolocation':
        val = ','.join([str(l) for l in val])
      return "<div class='ui small pointing below label'>{0}</div>{1}".format(label,
          tpl.get(item, tpl['default']).format(name, handler.locale.translate(item), val))
  return ''.join(['<div class="field">%s</div>' % process(t) for t in tuple])

def select_agent(handler, selected=''):
  if isinstance(selected, DBRef):
    selected = handler.db.dereference(selected)['_id']
  agent_list = handler.db.agent.find()
  item = "<div class='item' data-value='{_id}'>{name}</div>"
  menu = "".join([item.format(**a) for a in agent_list])
  return """
    <div class="field">
      <label>Assign to Agent</label>
      <div class="ui selection dropdown">
        <input type="hidden" name="agent" value="{1}" />
        <div class="default text">{1}</div>
        <i class="dropdown icon"></i>
        <div class="menu">{0}</div>
      </div>
    </div>""".format(menu, selected)

def role_filter(handler, user):
  return list(filter(lambda x:x!=user['role'], ['admin', 'editor']))[0]

def get_state_abbr(handler):
  return [
      ['阿拉巴马州','Alabama','AL'],
      ['阿拉斯加州','Alaska','AK'],
      ['阿利桑那州','Arizona','AZ'],
      ['阿肯色州','Arkansas','AR'],
      ['加利福尼亚州','California','CA'],
      ['科罗拉多州','Colorado','CO'],
      ['康涅狄格州','Connecticut','CT'],
      ['特拉华州','Delaware','DE'],
      ['佛罗里达州','Florida','FL'],
      ['乔治亚州','Georgia','GA'],
      ['夏威夷州','Hawaii','HI'],
      ['爱达荷州','Idaho','ID'],
      ['伊利诺斯州','Illinois','IL'],
      ['印第安纳州','Indiana','IN'],
      ['爱荷华州','Iowa','IA'],
      ['堪萨斯州','Kansas','KS'],
      ['肯塔基州','Kentucky','KY'],
      ['路易斯安那州','Louisiana','LA'],
      ['缅因州','Maine','ME'],
      ['马里兰州','Maryland','MD'],
      ['马萨诸塞州','Massachusetts','MA'],
      ['密歇根州','Michigan','MI'],
      ['明尼苏达州','Minnesota','MN'],
      ['密西西比州','Mississippi','MS'],
      ['密苏里州','Missouri','MO'],
      ['蒙大拿州','Montana','MT'],
      ['内布拉斯加州','Nebraska','NE'],
      ['内华达州','Nevada','NV'],
      ['新罕布什尔州','New hampshire','NH'],
      ['新泽西州','New jersey','NJ'],
      ['新墨西哥州','New mexico','NM'],
      ['纽约州','New york','NY'],
      ['北卡罗来纳州','North Carolina','NC'],
      ['北达科他州','North Dakota','ND'],
      ['俄亥俄州','Ohio','OH'],
      ['俄克拉荷马州','Oklahoma','OK'],
      ['俄勒冈州','Oregon','OR'],
      ['宾夕法尼亚州','Pennsylvania','PA'],
      ['罗得岛州','Rhode island','RL'],
      ['南卡罗来纳州','South carolina','SC'],
      ['南达科他州','South dakota','SD'],
      ['田纳西州','Tennessee','TN'],
      ['得克萨斯州','Texas','TX'],
      ['犹他州','Utah','UT'],
      ['佛蒙特州','Vermont','VT'],
      ['弗吉尼亚州','Virginia','VA'],
      ['华盛顿州','Washington','WA'],
      ['西弗吉尼亚州','West Virginia','WV'],
      ['威斯康辛州','Wisconsin','WI'],
      ['怀俄明州','Wyoming','WY']
  ]

import re
def sanitize(handler, raw_html, lens):
  cleanr = re.compile(r'<[^>]+>')
  cleantext = re.sub(cleanr,'', raw_html)[:lens]
  return cleantext

