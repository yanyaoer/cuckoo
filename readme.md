User Action
---
  login
  regsiter
    completing profile
      email, mobile, weibo
      ::age, address...

  search & browse
    houseDetail
      @HouseScheme
      image slider
      map viewer (?with neighborhood?)
      mark as starred
      ... @MORE ...

  mail recall
    price change, sold out ...
    ?new house suggest (mall-list subscribed before)


Workflow
---
manage
  role
    authorize admin
    action log

	wiki
    CURD
	house
    @HouseScheme
    image upload (with thumb generate, cdn disturb)
    update search indexer


HouseScheme
---
  {
    'address':'',
    'price':'',
    'property information':'',
    'main':{
      'size':{
        'bedrooms':'',
        'bathrooms':'',
        'garage':'',
        'square ft':'',
        'square metre':'',
        'lot size':'',
      },
      'value':{
        'price per sq/ft for this property':'',
        'average price per sq/ft for this area':'',
        'median home price':'',
      },
      'extra':{
        'year built':'',
        'school district':'',
        'mls number':'',
        'live date':'',
        'property type':'',
        'area location':'',
        'subdivision':'',
        'county':'',
        'status':'',
        'map page':'',
      }
    }
    'general':{
      'rooms':'',
      'stories':'',
      'bedrooms':'',
      'full bathrooms':'',
      'garage capacity':'',
      'parking description':'',
      'fireplaces':'',
      'square footage':'',
    },
    'exterior comments':{
      'exterior features':'',
      'fence':'',
      'fence description':'',
      'pool':'',
    },
    'interior comments':{
      'dining room level':'',
      'flooring type':'',
      'interior features':'',
      'kitchen level':'',
      'living room level':'',
      'rooms other':'',
    },
    'construction':{
      'exterior siding':'',
      'foundation type':'',
      'new construction':'',
      'style':'',
      'year built':'',
    },
    'equipment':{
      'appliances':'',
      'cooling':'',
      'heating':'',
      'equipment':'',
      'water heater type':'',
      'sewer type':'',
      'water type':'',
    },
    'property':{
      'listing agent':'',
      'listing provided courtesy of':'',
      'mls number':'',
    },
    'lot comments':{
      'view description':'',
      'waterfront description':'',
    },
    'additional information':{
      'high school':'',
      'tax amount':'',
    },
  }
