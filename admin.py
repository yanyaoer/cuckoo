import re
import json
import urllib
#from tornado.httpclient import HTTPError
from tornado import gen
from tornado.web import asynchronous
from tornado.escape import _unicode
from tornado.httpclient import AsyncHTTPClient

from bson.binary import Binary
from bson.objectid import ObjectId
from base import dispatch
from model import form, form2dict, get_agent
from auth import authenticated, require_role


mapping = {
  "address":{"properties":{"en":{"type":"string"}}},
  "agent":{"type":"string"},
  "area location":{"properties":{"cn":{"type":"string"},
  "en":{"type":"string"}}},
  "average price per sq/ft for this area":{"type":"long"},
  "bedroom":{"type":"long"},
  "city":{"properties":{
    "cn":{"type":"string"},
    "en":{"type":"string"}}},
  "construction year":{"type":"string"},
  "county":{"properties":{
    "cn":{"type":"string"},
    "en":{"type":"string"}}},
  "create_time":{"type":"date","format":"dateOptionalTime"},
  "dinning room floor":{"type":"long"},
  "drain":{"type":"string"},
  "else appliance":{"type":"string"},
  "else internal facility":{"type":"string"},
  "estate type":{"type":"string"},
  "external wall":{"type":"string"},
  "fencing":{"type":"string"},
  "foundation":{"type":"string"},
  "garage":{"type":"long"},
  "geolocation":{"type":"double"},
  "ground":{"type":"string"},
  "high":{"type":"string"},
  "hill":{"type":"string"},
  "household appliance":{"type":"string"},
  "internal facility":{"type":"string"},
  "kitchen floor":{"type":"long"},
  "lawn":{"type":"string"},
  "living room":{"type":"long"},
  "lot size":{"type":"string"},
  "median home price":{"type":"long"},
  "middle":{"type":"string"},
  "online":{"type":"long"},
  "pool":{"type":"string"},
  "price":{"properties":{
    "cn":{"type":"string"},
    "en":{"type":"long"}}},
  "price per sq/ft for this property":{"type":"long"},
  "primary":{"type":"string"},
  "property information":{"type":"string"},
  "pump":{"type":"string"},
  "refrigerator":{"type":"string"},
  "sale agent":{"type":"string"},
  "sale info provider":{"type":"string"},
  "sale start time":{"type":"string"},
  "sale status":{"type":"string"},
  "school area":{"type":"string"},
  "square footage":{"type":"string"},
  "square metre":{"type":"long"},
  "state":{"properties":{
    "abbr":{"type":"string"},
    "cn":{"type":"string"},
    "en":{"type":"string"}}},
  "step":{"type":"string"},
  "style":{"type":"string"},
  "subdivision":{"properties":{
    "cn":{"type":"string"},
    "en":{"type":"string"}}},
  "terrace":{"type":"string"},
  "title":{"type":"string"},
  "town":{"properties":{
    "cn":{"type":"string"},
    "en":{"type":"string"}}},
  "tree":{"type":"string"},
  "warmer":{"type":"string"},
  "water pool":{"type":"string"},
  "water supply":{"type":"string"},
  "water warmer":{"type":"string"},
  "watering can":{"type":"string"},
  "well":{"type":"string"}
}
#print(mapping)
#es.indices.put_mapping("cuckoo", {'properties':mapping}, ["house"])

class underground(dispatch):
  def send_json(self, obj):
    ret = obj if type(obj) == 'str' else self.jsondumps(obj)
    self.write(ret)
    self.finish()

  @require_role('admin', 'editor')
  @authenticated
  def dispatch(self, uri):
    # assign first user as admin
    if not self.db.admin.count():
      user = self.db.user.find_one(self.current_user)
      user['role'] = 'admin'
      self.db.admin.insert(user)
    super(underground, self).dispatch(uri)

  def _not_found(self):
    self.write('nothing here')

  def _(self):
    import subprocess
    cmd= subprocess.check_output(['git', 'log',
        #'--graph', '--abbrev-commit',
        '--pretty="%cr -%d %s"', '-n 10'])
        #'--pretty="%cr -%d %s <%an>"', '-n 10'])
    self.render('admin/index.jade',
      change = _unicode(cmd)
    )

  @require_role('admin')
  def _user(self):
    self.render('admin/user.jade',
      user_list = self.db.user.find(),
      admin_list = self.db.admin.group(['role'], None, {'list': []},
        'function(obj, prev) {prev.list.push(obj)}')
    )

  @require_role('admin')
  def _user_role(self, _id):
    user = self.db.user.find_one({'_id': ObjectId(_id)})
    role = self.get_argument('role')
    if role in ['admin', 'editor']:
      user['role'] = role
      self.db.admin.save(user)
    elif role == 'abandon':
      self.db.admin.remove(user)
    self.redirect('/underground/user')

  def _house(self, _id):
    step = int(self.get_argument('step', '0'))
    self.render('admin/house_form.jade',
      step = step,
      form = form,
      house = {} if _id=='new' else self.db.house.find_one({'_id': ObjectId(_id)}),
    )

  def _house_list(self):
    q = {'$nor': [{'state': 'CLOSED'}]}
    get_agent(self, amand=q)
    self.render('admin/house_list.jade',
      form = form,
      house_list = self.db.house.find({}).sort('_id', -1),
    )

  def house_data(self, d, k, v=None):
    if not v or v=='None':
      return
    ko = urllib.parse.unquote_plus(k)
    ks = ko.split('.')
    #print(k, urllib.parse.unquote_plus(k))
    v = int(v) if v.isdigit() else urllib.parse.unquote_plus(v).strip()
    if len(ks) == 1:
      if ko == 'geolocation':
        v = [float(l) for l in v.split(',')]
      elif ko == 'state':
        l = v.split('-')
        v = dict(cn=l[0], en=l[1][0].upper()+l[1][1:].lower(), abbr=l[2].upper())
      d[ko] = v
    else:
      d[ks[0]][ks[1]] = v

  @asynchronous
  @gen.coroutine
  def put_house_online(self, _id):
    url = 'http://127.0.0.1:9200/cuckoo/house/'+_id
    d = self.db.house.find_one({'_id':ObjectId(_id)})
    d['online'] = [1, 0][d.get('online', 0)]
    request = AsyncHTTPClient()

    def handle_request(req):
      if req.error:
        body = json.loads(_unicode(req.body))
        self.send_json(body)
      else:
        msg = {'text':'offline' if d.get('online') == 0 else 'online'}
        self.db.house.save(d)
        self.send_json(msg)

    if d['online']:
      yield request.fetch(url, handle_request, method="PUT", body=self.jsondumps(d))
    else:
      yield request.fetch(url, handle_request, method="DELETE")

  def put_house(self, _id):
    d = self.db.house.find_one({'_id':ObjectId(_id)})
    b = self.request.body.decode('utf8')
    for l in b.split('&'):
      a = l.split('=')
      self.house_data(d, a[0], a[1])

    self.db.house.save(d)
    step = int(self.get_argument('step', '0'))
    step = 0 if step == 9 else step+1
    self.send_json({'redir':'/underground/house/{id}?step={step}'.format(id=str(_id),step=step)})

  def post_house(self, id=None):
    d = form2dict()
    oid = ObjectId()
    d.update(**dict(_id=oid,create_time=oid.generation_time))

    for k,v in self.request.arguments.items():
      v = _unicode(v[-1])
      self.house_data(d, k, v)

    _id = self.db.house.insert(d)
    step = int(self.get_argument('step', '0'))
    step = 0 if step == 9 else step+1
    self.send_json({'redir':'/underground/house/{id}?step={step}'.format(id=str(_id),step=step)})

  def _article(self, _id=None):
    self.render('admin/article_form.jade',
      article = {} if _id=='new' else self.db.article.find_one({'_id': ObjectId(_id)})
    )

  def _article_list(self):
    q = {'$nor': [{'state': 'CLOSED'}]}
    get_agent(self, amand=q)
    self.render('admin/article_list.jade',
      article_list = self.db.article.find(q)
    )

  def put_article(self, _id):
    q = {'_id': ObjectId(_id)}
    update = {
      'title': self.get_argument('title', None),
      'agent': get_agent(self),
      'state': self.get_argument('state', None),
      'content': self.get_argument('content', None),
      'tag': self.parse_tag(),
    }
    self.db.article.update(q, {'$set': update}, safe=True)
    self.send_json({'_id':_id})

  def post_article(self, slash='/'):
    _id = self.db.article.insert({
      'title': self.get_argument('title'),
      'agent': get_agent(self),
      'content': self.get_argument('content'),
      'tag': self.parse_tag(),
    })
    self.send_json({'_id':_id})

  def _agent(self, _id=None):
    self.render('admin/agent_form.jade',
      agent = {} if _id=='new' else self.db.agent.find_one({'_id': ObjectId(_id)})
    )

  def _agent_list(self):
    q = {'$nor': [{'state': 'CLOSED'}]}
    self.render('admin/agent_list.jade',
      agent_list = self.db.agent.find(q)
    )

  def put_agent(self, _id):
    q = {'_id': ObjectId(_id)}
    d = {k:self.get_argument(k, '') for k in ['name', 'title', 'tel', 'email', 'detail']}
    photo = self.request.files['photo']
    if photo:
      d['photo'] = [self.fs.put(Binary(f['body']), content_type=f['content_type']) for f in photo]
    self.db.article.update(q, {'$set': d}, safe=True)
    self.redirect('/underground/agent/'+str(_id))

  def post_agent(self, slash='/'):
    d = {k:self.get_argument(k, '') for k in ['name', 'title', 'tel', 'email', 'detail']}
    photo = self.request.files['photo']
    if photo:
      d['photo'] = [self.fs.put(Binary(f['body']), content_type=f['content_type']) for f in photo]
    _id = self.db.agent.insert(d)
    self.redirect('/underground/agent/'+str(_id))

  # STATIC_UPLOAD
  def _file(self, slash='/'):
    f = self.fs.find()
    if self.get_argument('purge', None):
      for x in f:
        if type(x._id) == str and re.search('/\d+x\d+$', x._id):
          self.fs.delete(x._id)
      self.redirect('/underground/file/')
    else:
      self.render('admin/file.jade', file_list = f)

  def post_file(self):
    files = self.request.files['file']
    res = []
    for f in files:
      f = self.fs.put(Binary(f['body']), content_type=f['content_type'])
      res.append(f)
    self.send_json(res)

  def _file_delete(self, _id, size=None):
    key = _id +'/'+ size if size else ObjectId(_id)
    self.fs.delete(key)
    self.redirect('/underground/file')

  def _gallery(self):
    q = dict(house_id=ObjectId(self.get_argument('house_id')))
    self.render('admin/gallery.jade',
      gallery_list = list(self.db.gallery.find(q).sort('order', -1))
    )

  def post_gallery(self):
    house_id = self.get_argument('house_id')
    files = self.request.files['file']
    for f in files:
      f = self.fs.put(Binary(f['body']), content_type=f['content_type'])
      self.db.gallery.save({
        '_id': ObjectId(f),
        'house_id': ObjectId(house_id),
        #'tag': self.parse_tag(),
      })
    self.redirect('/underground/gallery?house_id='+house_id)

  def put_gallery(self):
    house_id = self.get_argument('house_id')
    q = dict(house_id=ObjectId(house_id))
    order = self.get_arguments('order')
    for i, photo in enumerate(self.db.gallery.find(q).sort('order', -1)):
      photo['order'] = int(order[i])
      self.db.gallery.save(photo)
    self.redirect('/underground/gallery?house_id='+house_id)
