import json
#import pyes
from tornado import gen
from tornado.options import options
from tornado.escape import _unicode
from tornado.web import asynchronous
from tornado.httpclient import AsyncHTTPClient
from bson.objectid import ObjectId
from base import base, wrap_houselist
from auth import get_current_user_role


class search(base):
  @asynchronous
  @gen.coroutine
  def get(self, api):
    t = self.get_argument('t', 'house')
    q = self.get_argument('q', '*')
    p = int(self.get_argument('p', 1)) -1
    order = ["desc", "asc"][int(self.get_argument('order', 0))]
    sort_key = self.get_argument('sort', None) # 'square footage'
    state = self.get_argument('state', None)
    sort_hash = {
      'create_time': 'create_time',
      'size': 'square metre',
      'price': 'price.en',
    }
    if t not in ['house', 'article']:
      t = 'house'
    if not q:
      q = '*'

    dsl = {
      "from": p*options.size,
      "size": options.size,
      "query": {
        "query_string": {
          "default_operator": "AND",
          "query": q
        }
      }
    }
    if state:
      dsl['query']['query_string']['query'] ="{0} state.abbr:'{1}'".format(q, state)
    if sort_key:
      dsl['sort']={
        "_script": {
          "script": "s = doc['"+sort_hash.get(sort_key, 'create_time')+"'].value; n = org.elasticsearch.common.primitives.Ints.tryParse(s); if (n != null) { String.format(\"%010d\",n)} else { s }",
          "type": "string",
          "order" : order
        }
      }

    #extra = ''
    extra = '' if t=='house' else '_'+t
    url = 'http://127.0.0.1:9200/cuckoo'+extra+'/{0}/_{1}?'
    req = yield AsyncHTTPClient().fetch(url.format(t, 'search'), method="POST", body=self.jsondumps(dsl))
    res = json.loads(_unicode(req.body))
    self.output(res, t, api)

  @asynchronous
  @gen.coroutine
  def output(self, obj, t='house', api=False):
    res = [s['_source'] for s in obj['hits']['hits']]
    if t == 'house':
      ret = wrap_houselist(self, list(res))
    elif t == 'article':
      ret = {'article_list': []}
      for article in res:
        article['agent'] = self.db.agent.find_one(ObjectId(article['agent']['id']))
        ret['article_list'].append(article)
    ret['page_max'] = (int(obj['hits']['total']) + options.size - 1)//options.size
    if api:
      self.set_header('Content-Type', 'application/json')
      self.write(self.jsondumps(ret))
      self.finish()
    else:
      #print(self.render_string('_'+t+'_list.jade', **ret))
      self.render('_'+t+'_list.jade',
          user_role=get_current_user_role(self),
          **ret)
