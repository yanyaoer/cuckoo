#!/bin/bash
#[elasticsearch-mongodb-river]
#http://www.nosqldb.cn/1368778445826.html
#https://coderwall.com/p/sy1qcw
#https://github.com/richardwilly98/elasticsearch-river-mongodb

#$ ./bin/plugin --install com.github.richardwilly98.elasticsearch/elasticsearch-river-mongodb/2.0.0

#$ service elasticsearch restart

#vim path/mongodb.conf

	#replSet=rs0
	#oplogSize=100

#$ mongo

	#rs.initiate( {"_id" : "rs0", "version" : 1, "members" : [ { "_id" : 0, "host" : "127.0.0.1:27017" } ]})
	#rs.slaveOk()

#$ service mongodb restart

curl -XPUT 'http://localhost:9200/_river/cuckoo_house/_meta' -d '{
    "type": "mongodb",
    "mongodb": {
        "db": "cuckoo",
        "collection": "house",
        "script": "if( ctx.document.state == 'CLOSED' ) { ctx.deleted = true; }"
    },
    "index": {
        "name": "cuckoo",
        "type": "house"
    }
}'

curl -XPUT 'http://localhost:9200/_river/cuckoo_article/_meta' -d '{
    "type": "mongodb",
    "mongodb": {
        "db": "cuckoo",
        "collection": "article",
        "script": "if( ctx.document.state == 'CLOSED' ) { ctx.deleted = true; }"
    },
    "index": {
        "name": "cuckoo",
        "type": "article"
    }
}'

curl -XPUT 'http://localhost:9200/_river/cuckoo_user/_meta' -d '{
    "type": "mongodb",
    "mongodb": {
        "db": "cuckoo",
        "collection": "user",
        "script": "if( ctx.document.state == 'CLOSED' ) { ctx.deleted = true; }"
    },
    "index": {
        "name": "cuckoo",
        "type": "user"
    }
}'

curl -XPUT 'http://localhost:9200/_river/cuckoo/_meta' -d '{
    "type": "mongodb",
        "mongodb": {
        "db": "cuckoo",
            "options": {
                "drop_collection": true,
		"exclude_fields": ["tax amount", "MLS", "floor", "room"],
                "import_all_collections": true
            }
        },
    "index": {
        "name": "cuckoo"
    }
}'
