#!/bin/bash
curl -XDELETE 'http://127.0.0.1:9200/_river/cuckoo'
curl -XDELETE 'http://127.0.0.1:9200/cuckoo'
curl -XPUT 'http://localhost:9200/_river/cuckoo/_meta' -d '{
    "type": "mongodb",
    "mongodb": {
      "db": "cuckoo",
      "collection": "house",
      "options": {
        "drop_collection": true,
        "exclude_fields": ["tax amount", "MLS", "floor", "room", "bathroom"]
      }
    },
    "index": {
        "name": "cuckoo",
        "type": "house"
    }
}'
