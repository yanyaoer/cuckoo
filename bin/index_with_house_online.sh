#!/bin/bash
curl -XDELETE 'http://127.0.0.1:9200/cuckoo'
curl -XDELETE 'http://127.0.0.1:9200/cuckoo_user'
curl -XDELETE 'http://127.0.0.1:9200/cuckoo_article'


curl -XPUT 'http://127.0.0.1:9200/cuckoo'
curl -XPUT 'http://127.0.0.1:9200/cuckoo_user'
curl -XPUT 'http://127.0.0.1:9200/cuckoo_article'
curl -XPUT 'http://localhost:9200/cuckoo/house/_mapping' -d '
{
	"tweet" : {
		 "properties": {
			"construction year": {"type": "string"},
			"garage": {"type": "long"},
			"ground": {"type": "string"},
			"household appliance": {"type": "string"},
			"online": {"type": "long"},
			"kitchen floor": {"type": "long"},
			"area location": {"properties": {"cn": {"type": "string"},
			"en": {"type": "string"}}},
			"geolocation": {"type": "double"},
			"lawn": {"type": "string"},
			"lot size": {"type": "string"},
			"terrace": {"type": "string"},
			"sale status": {"type": "string"},
			"median home price": {"type": "long"},
			"external wall": {"type": "string"},
			"warmer": {"type": "string"},
			"create_time": {"format": "dateOptionalTime", "type": "date"},
			"city": {"properties": {"cn": {"type": "string"},
			"en": {"type": "string"}}},
			"price per sq/ft for this property": {"type": "long"},
			"water pool": {"type": "string"},
			"county": {"properties": {"cn": {"type": "string"},
			"en": {"type": "string"}}},
			"well": {"type": "string"},
			"primary": {"type": "string"},
			"price": {"properties": {"cn": {"type": "string"},
			"en": {"type": "long"}}},
			"living room": {"type": "long"},
			"state": {"properties": {"cn": {"type": "string"},
			"en": {"type": "string"},
			"abbr": {"type": "string"}}},
			"fencing": {"type": "string"},
			"refrigerator": {"type": "string"},
			"sale agent": {"type": "string"},
			"school area": {"type": "string"},
			"agent": {"type": "string"},
			"property information": {"type": "string"},
			"else appliance": {"type": "string"},
			"pool": {"type": "string"},
			"internal facility": {"type": "string"},
			"else internal facility": {"type": "string"},
			"average price per sq/ft for this area": {"type": "long"},
			"subdivision": {"properties": {"cn": {"type": "string"},
			"en": {"type": "string"}}},
			"step": {"type": "string"},
			"high": {"type": "string"},
			"middle": {"type": "string"},
			"bedroom": {"type": "long"},
			"tree": {"type": "string"},
			"style": {"type": "string"},
			"hill": {"type": "string"},
			"address": {"properties": {"en": {"type": "string"}}},
			"estate type": {"type": "string"},
			"square metre": {"type": "long"},
			"town": {"properties": {"cn": {"type": "string"},
			"en": {"type": "string"}}},
			"square footage": {"type": "string"},
			"sale start time": {"type": "string"},
			"foundation": {"type": "string"},
			"drain": {"type": "string"},
			"title": {"type": "string"},
			"pump": {"type": "string"},
			"water supply": {"type": "string"},
			"watering can": {"type": "string"},
			"sale info provider": {"type": "string"},
			"water warmer": {"type": "string"},
			"dinning room floor": {"type": "long"}
		}
	}
}
'

curl -XPUT 'http://localhost:9200/_river/cuckoo_article/_meta' -d '{
    "type": "mongodb",
    "mongodb": {
        "db": "cuckoo",
        "collection": "article"
    },
    "index": {
        "name": "cuckoo_article",
        "type": "article"
    }
}'

curl -XPUT 'http://localhost:9200/_river/cuckoo_user/_meta' -d '{
    "type": "mongodb",
    "mongodb": {
        "db": "cuckoo",
        "collection": "user"
    },
    "index": {
        "name": "cuckoo_user",
        "type": "user"
    }
}'
