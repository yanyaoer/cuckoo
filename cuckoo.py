import os
import pyes
import pymongo
from gridfs import GridFS
from tornado import ioloop, locale
from tornado.web import Application
from tornado.options import options, parse_config_file, parse_command_line
from pyjade.ext.tornado import patch_tornado
from handler import router
import uimethods

_dir = os.path.dirname(__file__)
parse_config_file(os.path.join(_dir, 'setting.py'))
parse_command_line()
patch_tornado()


class app(Application):
  def __init__(self):
    settings = dict(
      static_path=os.path.join(_dir, "static"),
      template_path=os.path.join(_dir, "tpl"),
      ui_methods=uimethods,
      cookie_secret='tPD0/OoZS/25XrC0x12pAjbNet+gBk3bmnUTLgmwZYM=',
      debug=options.debug,
      gzip=True,
    )
    locale.load_translations(os.path.join(_dir, "lang"))
    Application.__init__(self, router, **settings)
    self._db = pymongo.Connection()[options.package]
    self._fs = GridFS(self._db)
    self._es = pyes.ES('127.0.0.1:9200', default_indices=[options.package])


def main():
  server = app()
  if not options.debug:
    import yaml
    import logging
    import logging.config
    import newrelic.agent
    from raven.contrib.tornado import AsyncSentryClient
    sentry_url = options.sentry_url
    server.sentry_client = AsyncSentryClient(sentry_url)
    newrelic.agent.initialize('newrelic.ini')
    logging.info('hello cuckoo')
    logging.config.dictConfig(yaml.load(open('logging.yaml', 'r')))

  server.listen(options.port)
  ioloop.IOLoop.current().start()


if __name__ == "__main__":
  main()
