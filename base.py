import re
import json
import hashlib
import datetime
import tornado.web
#from tornado.options import options
from tornado.httpclient import HTTPError
#from gridfs import GridFS
from bson.objectid import ObjectId
from bson.dbref import DBRef
from raven.contrib.tornado import SentryMixin
from model import form


class base(SentryMixin, tornado.web.RequestHandler):
  def initialize(self, **kwargs):
    self.es = self.application._es
    self.db = self.application._db
    self.fs = self.application._fs
    self.ui.update({
      'lang': lambda: json.dumps(self.locale.get('zh_CN').translations.get('unknown', {})),
      'year': lambda: datetime.datetime.now().year,
      'is_thumb': lambda oid: re.findall('/\d+x\d+$', oid)
      #'user': lambda : self.jsondumps(self.current_user)
    })

  def sanitized_user(self, user):
    for k in ['_id', 'password']:
      del user[k]
    return user

  def get_current_user(self):
    uid = self.get_secure_cookie("user")
    if uid:
      user = self.db.user.find_one({'_id': ObjectId(uid.decode('utf8'))})
      return self.sanitized_user(user)
    else:
      return None

  def hashed_password(self):
    passwd = self.get_argument('password')
    #TODO password strong checking
    if not passwd:
      raise HTTPError(500, message='password not valid')
    return hashlib.md5(passwd.encode('utf-8')).hexdigest()

  def get_login_url(self):
    return '/login'

  def get_user_locale(self):
    return tornado.locale.get('zh_CN')

  def jsondumps(self, data):
    def type_handler(obj):
      if isinstance(obj, datetime.datetime):
        return obj.isoformat()
      elif isinstance(obj, ObjectId):
        return str(obj)
      elif isinstance(obj, DBRef):
        return self.db.dereference(obj)
      else:
        return None
    return json.dumps(data, default=type_handler)

  def house_json(self, json=True):
    res = {field[0]: field[1] if len(field[1:]) == 1 else field[1:] for field in form}
    return self.jsondumps(res) if json else res


class dispatch(base):
  def dispatch(self, uri):
    method = self.get_argument('_method', self.request.method).lower()
    prefix = '' if method == 'get' else method
    array = uri.split('/')
    for k in range(len(array)):
      lens = len(array)-k
      _attr = '_'.join([prefix]+array[:lens])
      print(_attr)
      if hasattr(self, _attr):
        return getattr(self, _attr)(*array[lens:])
    else:
      return self._not_found()

  def _not_found(self):
    pass

  def _execute_method(self):
    if not self._finished:
      #print(self.path_args)
      method = self.dispatch
      self._when_complete(method(*self.path_args, **self.path_kwargs),
                                          self._execute_finish)

  def parse_tag(self):
    tag_str = self.get_argument('tag', '')
    return [x.strip() for x in tag_str.split(',')] if tag_str else None


def wrap_houselist(handler, house_list):
  agent_list = []
  for house in house_list:
    house_id = house['_id']
    if isinstance(house_id, str):
      house_id = ObjectId(house_id)
    house['gallery'] = list(handler.db.gallery.find({'house_id': house_id}).sort('order', -1).limit(4)) or None
    if house.get('agent', None):
      agent = handler.db.agent.find_one({'_id':ObjectId(house['agent'])})
      if agent and agent not in agent_list:
        agent_list.append(agent)
  return {
    'house_list': house_list,
    'agent_list': agent_list,
    'article_list': list(handler.db.article.find().limit(5))
  }
