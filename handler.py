import io
from PIL import Image, ImageOps
from tornado.options import options
#from tornado import gen
from tornado.httpclient import HTTPError
from bson.objectid import ObjectId
from base import base, dispatch, wrap_houselist
from search import search
from admin import underground
from auth import get_current_user_role


class index(base):
  def get(self, uri):
    array = uri.split('/')
    for k in range(len(array)):
      lens = len(array)-k
      _attr = '_'+'_'.join(array[:lens])
      if hasattr(ctrl, _attr):
        #kargs = getattr(ctrl, _attr)(self, *array[lens:])
        if _attr == '_house':
          #print(array[-1])
          kargs = {
            'house': ctrl._house(self, array[-1]),
            'hbase': self.house_json(False)
          }
        elif _attr == '_article':
          kargs = {'article': getattr(ctrl, _attr)(self, *array[lens:])}
        else:
          kargs = getattr(ctrl, _attr)(self, *array[lens:])
        self.render(_attr+'.jade' if _attr != '_' else '_index.jade',
            user_role=get_current_user_role(self),
            **kargs)
        break
    else:
      self.render('404.jade')


class api(dispatch):
  def dispatch(self, uri):
    method = self.get_argument('_method', self.request.method).lower()
    prefix = '' if method == 'get' else method
    array = uri.split('/')
    for k in range(len(array)):
      lens = len(array)-k
      _attr = '_'.join([prefix]+array[:lens])
      print(_attr)
      if hasattr(ctrl, _attr):
        res = getattr(ctrl, _attr)(self, *array[lens:])
        self.set_header('Content-Type', 'application/json')
        self.write(self.jsondumps(res))
        self.finish()
    else:
      return self._not_found()

  def write_error(self, status_code, **kwargs):
    ret = {
      'error': status_code,
      'info': 'server error',
    }
    if options.debug:
      import traceback
      ret['info'] = traceback.format_exception(*kwargs["exc_info"])
    self.write(self.jsondumps(ret))
    self.finish()


class ctrl(dict):
  def _not_found(self):
    return {'error': '404 not found.'}

  def _check(self):
    email = self.get_argument('mail', None)
    if email:
      if len(email) == 0 or self.db.user.find_one({ 'email': email }):
        return {'msg': '该邮箱已注册'}
      else:
        return {'msg': ''}

  # USER
  def post_register(self):
    email = self.get_argument('email')
    #TODO emailformat checking
    if len(email) == 0 or self.db.user.find_one({ 'email': email }):
      self.set_status(500)
      return {'error': 'this email not availiable.'}
    else:
      uid = self.db.user.insert({
        'email': email,
        'phone': self.get_argument('phone'),
        'password': self.hashed_password()
      })
      self.set_secure_cookie("user", str(uid))
      return {'_id': uid}

  def post_login(self):
    user = self.db.user.find_one({
      'email': self.get_argument('email'),
      'password': self.hashed_password()
    })
    if not user:
      return {'error':'email or password not validated'}
    self.clear_cookie("user")
    self.set_secure_cookie("user", str(user['_id']))
    return {'_id': user['_id']}

  def _logout(self):
    self.clear_cookie("user")
    return {'succ':'logout success'}

  def _user(self, _id=None):
    return self.sanitized_user(self.db.user.find_one({'_id':ObjectId(_id)})) \
      if _id else self.get_current_user()

  def _(self):
    ret = wrap_houselist(self, list(self.db.house.find({'online':1}).limit(4)))
    ret['state_list'] = self.db.house.aggregate([
      {'$match': {'online':1}},
      {'$group': {
        '_id': {'cn':'$state.cn', 'abbr':'$state.abbr'},
        'total': {'$sum':1},
      }}]
    )['result']
    return ret

  def _house(self, _id=None):
    if _id:
      house = self.db.house.find_one({'_id': ObjectId(_id), 'online':1})
      if not house:
        raise HTTPError(404, 'house not found')
      house['gallery'] = list(self.db.gallery.find({'house_id': ObjectId(_id)}).sort('order', -1)) or None
      house['neighbor'] = []
      #for near in self.db.house.find({'geolocation':{ '$near': house['geolocation'], '$maxDistance':5 }}, {'address':1}).limit(5):
        #near['photo'] = self.db.gallery.find_one({'house_id': ObjectId(_id)})
        #house['neighbor'].append(near)
      if house.get('agent', None):
        house['agent'] = self.db.agent.find_one({'_id':ObjectId(house['agent'])})
      return house
    else:
      return wrap_houselist(self, list(self.db.house.find({'online':1})))

  # ARTICLE
  def _article(self, _id=None):
    if _id:
      return self.db.article.find_one({'_id': ObjectId(_id)})
    else:
      return list(self.db.article.find())


class assets(base):
  def send_img(self, _id):
    f = self.fs.get(_id)
    ret = f.read()
    self.set_header('Content-Type', f.content_type)
    self.set_header('Content-Length', len(ret))
    self.finish(ret)

  def get(self, _id, size=None):
    if not self.fs.exists(ObjectId(_id)):
      raise HTTPError(404, 'file not found')
    if size:
      return self._resize(_id,size)
    self.send_img(ObjectId(_id))

  def _resize(self, _id, size):
    buffer = io.BytesIO()
    w,h = size[1:].split('x')
    rid = _id+size
    if not self.fs.exists(rid):
      origin = self.fs.get(ObjectId(_id))
      image = Image.open(origin)
      s = int(w), int(h)
      resized = ImageOps.fit(image, s, Image.ANTIALIAS)
      resized.save(buffer, format='jpeg', optimize=True)
      self.fs.put(buffer.getvalue(), content_type='IMAGE/PNG', _id=rid)
    self.send_img(rid)


router = [
  (r"/assets/(\w+)(/\w+)?", assets),
  (r"/?(api/)?search", search),
  (r"/api/(.*)", api),
  (r"/underground/(.*)", underground),
  (r"/(.*)", index),
]
